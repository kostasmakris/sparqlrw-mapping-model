# README #

This is the implementation of the *SPARQL–RW mapping model*, a model for the expression of mappings between ontology schemas in the context of ontology based mediators. It is based on Description Logics and it is capable of describing a great variety of mapping types between OWL ontologies, providing high flexibility and satisfying different system requirements and user needs. The mapping model is able to support well-known mapping formalisms, including GAV, LAV, and GLAV, satisfying strong data integration requirements for query rewriting efficiency and extensibility to new sources.

In order to assist the mapping definition process and support mapping maintenance and interoperability, the API exposes functionality for XML serialization, deserialization and syntax validation. Furthermore, it provides methods for performing mapping inference and inconsistency identification in a given set of mappings and ontology schemas. 

*More information about the SPARQL-RW Framework is available at:* [*http://dias.library.tuc.gr/view/17491*](http://dias.library.tuc.gr/view/17491)