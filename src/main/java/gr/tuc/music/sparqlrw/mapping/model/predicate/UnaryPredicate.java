/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.predicate;

import gr.tuc.music.sparqlrw.mapping.model.value.Value;

/**
 * A unary predicate.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class UnaryPredicate extends Predicate {

    private final Value value;

    /**
     * Creates a UnaryPredicate object using specified values.
     *
     * @param operator A binary operator.
     * @param value    A class instance or data value.
     */
    public UnaryPredicate(Operator operator, Value value) {
        super(operator);
        this.value = value;
    }

    public Value getValue() {
        return value;
    }

    @Override
    public <T1, T2> T1 accept(PredicateVisitor<T1, T2> visitor, T2 o) {
        return visitor.visit(this, o);
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 17 * hash + (this.value != null ? this.value.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final UnaryPredicate other = (UnaryPredicate) obj;
        if (this.value != other.value && (this.value == null || !this.value.equals(other.value))) {
            return false;
        }
        return true;
    }
}
