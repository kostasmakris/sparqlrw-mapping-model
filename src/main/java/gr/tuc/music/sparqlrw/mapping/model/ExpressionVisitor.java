/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import gr.tuc.music.sparqlrw.mapping.model.concept.ClassCardinalityRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassDifference;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassEnumeration;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistQuantification;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassIntersection;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUnion;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyTransform;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyInverse;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyTransitive;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;

/**
 * A visitor interface for traversing an expression.
 *
 * @param <T> The return type of the visit methods.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public interface ExpressionVisitor<T> {

    public T visit(ClassUri o);

    public T visit(ClassUnion o);

    public T visit(ClassIntersection o);

    public T visit(ClassDifference o);

    public T visit(ClassEnumeration o);

    public T visit(ClassCardinalityRestriction o);

    public T visit(ClassExistPredRestriction o);

    public T visit(ClassExistQuantification o);

    public T visit(OPropertyUri o);

    public T visit(OPropertyUnion o);

    public T visit(OPropertyIntersection o);

    public T visit(OPropertyDifference o);

    public T visit(OPropertyComposition o);

    public T visit(OPropertyInverse o);

    public T visit(OPropertyTransitive o);

    public T visit(OPropertyRestriction o);

    public T visit(OPropertyExistPredRestriction o);

    public T visit(DPropertyUri o);

    public T visit(DPropertyUnion o);

    public T visit(DPropertyIntersection o);

    public T visit(DPropertyDifference o);

    public T visit(DPropertyComposition o);

    public T visit(DPropertyTransform o);

    public T visit(DPropertyRestriction o);

    public T visit(InstanceUri o);

    public T visit(Datatype o);
}
