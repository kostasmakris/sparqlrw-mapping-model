/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.concept;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import gr.tuc.music.sparqlrw.mapping.model.predicate.Predicate;
import gr.tuc.music.sparqlrw.mapping.model.property.PropertyExpression;
import java.util.ArrayList;
import java.util.List;

/**
 * An existential predicate restriction on property values.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class ClassExistPredRestriction extends ClassExpression {

    private final List<PropertyExpression> operandList;
    private final Predicate predicate;

    /**
     * Creates a ClassExistPredRestriction object using specified values.
     *
     * @param operandList A list of property expressions.
     * @param predicate   A predicate .
     */
    public ClassExistPredRestriction(List<PropertyExpression> operandList, Predicate predicate) {
        this.operandList = new ArrayList<PropertyExpression>(operandList);
        this.predicate = predicate;
    }

    public List<PropertyExpression> getOperandList() {
        return operandList;
    }

    public Predicate getPredicate() {
        return predicate;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        for (PropertyExpression expr : operandList) {
            resourceList.addAll(expr.listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.operandList != null ? this.operandList.hashCode() : 0);
        hash = 47 * hash + (this.predicate != null ? this.predicate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClassExistPredRestriction other = (ClassExistPredRestriction) obj;
        if (this.operandList != other.operandList && (this.operandList == null || !this.operandList.equals(other.operandList))) {
            return false;
        }
        if (this.predicate != other.predicate && (this.predicate == null || !this.predicate.equals(other.predicate))) {
            return false;
        }
        return true;
    }
}
