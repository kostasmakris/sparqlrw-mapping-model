/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import gr.tuc.music.sparqlrw.mapping.model.concept.ClassCardinalityRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassDifference;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassEnumeration;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistQuantification;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExpression;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassIntersection;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassMapping;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUnion;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.condition.AndCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.BasicCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.Condition;
import gr.tuc.music.sparqlrw.mapping.model.condition.ConditionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.condition.NotCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.OrCondition;
import gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceMapping;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.predicate.BinaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.value.DataValue;
import gr.tuc.music.sparqlrw.mapping.model.predicate.PredicateVisitor;
import gr.tuc.music.sparqlrw.mapping.model.predicate.UnaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.property.PropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyTransform;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyInverse;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyTransitive;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;

/**
 * A basic Visitor implementation.
 *
 * @param <T1> The return type of the mapping model visit methods.
 * @param <T2> The return type of the mapping visit methods.
 * @param <T3> The return type of the expression visit methods.
 * @param <T4> The return type of the condition visit methods.
 * @param <T5> The input type of the condition visit methods.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class VisitorTemplate<T1, T2, T3, T4, T5, T6, T7> implements MappingModelVisitor<T1>, MappingVisitor<T2>, ExpressionVisitor<T3>, ConditionVisitor<T4, T5>, PredicateVisitor<T6, T7> {

    public T1 visit(MappingModel o) {
        if (o.getGlobalOntology() != null) {
            o.getGlobalOntology().accept(this);
        }
        for (Ontology ontology : o.getLocalOntologyList()) {
            ontology.accept(this);
        }
        for (Mapping mapping : o.getMappingList()) {
            mapping.accept(this);
        }

        return null;
    }

    public T1 visit(Ontology o) {
        return null;
    }

    public T2 visit(ClassMapping o) {
        if (o.getExpr1() != null) {
            o.getExpr1().accept(this);
        }
        if (o.getExpr2() != null) {
            o.getExpr2().accept(this);
        }

        return null;
    }

    public T2 visit(OPropertyMapping o) {
        if (o.getExpr1() != null) {
            o.getExpr1().accept(this);
        }
        if (o.getExpr2() != null) {
            o.getExpr2().accept(this);
        }

        return null;
    }

    public T2 visit(DPropertyMapping o) {
        if (o.getExpr1() != null) {
            o.getExpr1().accept(this);
        }
        if (o.getExpr2() != null) {
            o.getExpr2().accept(this);
        }

        return null;
    }

    public T2 visit(InstanceMapping o) {
        if (o.getExpr1() != null) {
            o.getExpr1().accept(this);
        }
        if (o.getExpr2() != null) {
            o.getExpr2().accept(this);
        }

        return null;
    }

    public T3 visit(ClassUri o) {
        return null;
    }

    public T3 visit(ClassUnion o) {
        for (ClassExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(ClassIntersection o) {
        for (ClassExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(ClassDifference o) {
        for (ClassExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(ClassEnumeration o) {
        for (InstanceUri instance : o.getOperandList()) {
            instance.accept(this);
        }

        return null;
    }

    public T3 visit(ClassCardinalityRestriction o) {
        if (o.getProperty() != null) {
            o.getProperty().accept(this);
        }
        if (o.getQuantifier() != null) {
            o.getQuantifier().accept(this);
        }
        if (o.getCardinality() != null) {
            o.getCardinality().accept(this, null);
        }

        return null;
    }

    public T3 visit(ClassExistPredRestriction o) {
        for (PropertyExpression expr : o.getOperandList()) {
            expr.accept(this);
        }
        if (o.getPredicate() != null) {
            o.getPredicate().accept(this, null);
        }

        return null;
    }

    public T3 visit(ClassExistQuantification o) {
        if (o.getProperty() != null) {
            o.getProperty().accept(this);
        }
        if (o.getQuantifier() != null) {
            o.getQuantifier().accept(this);
        }

        return null;
    }

    public T3 visit(OPropertyUri o) {
        return null;
    }

    public T3 visit(OPropertyUnion o) {
        for (OPropertyExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(OPropertyIntersection o) {
        for (OPropertyExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(OPropertyDifference o) {
        for (OPropertyExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(OPropertyComposition o) {
        for (OPropertyExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(OPropertyInverse o) {
        if (o.getOperand() != null) {
            o.getOperand().accept(this);
        }

        return null;
    }

    public T3 visit(OPropertyTransitive o) {
        if (o.getOperand() != null) {
            o.getOperand().accept(this);
        }

        return null;
    }

    public T3 visit(OPropertyRestriction o) {
        if (o.getProperty() != null) {
            o.getProperty().accept(this);
        }
        if (o.getDomain() != null) {
            o.getDomain().accept(this);
        }
        if (o.getRange() != null) {
            o.getRange().accept(this);
        }

        return null;
    }

    public T3 visit(OPropertyExistPredRestriction o) {
        for (PropertyExpression expr : o.getOperand1List()) {
            expr.accept(this);
        }
        for (PropertyExpression expr : o.getOperand2List()) {
            expr.accept(this);
        }
        if (o.getPredicate() != null) {
            o.getPredicate().accept(this, null);
        }

        return null;
    }

    public T3 visit(DPropertyUri o) {
        return null;
    }

    public T3 visit(DPropertyUnion o) {
        for (DPropertyExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(DPropertyIntersection o) {
        for (DPropertyExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(DPropertyDifference o) {
        for (DPropertyExpression expr : o.getOperandList()) {
            expr.accept(this);
        }

        return null;
    }

    public T3 visit(DPropertyComposition o) {
        for (OPropertyExpression expr : o.getInitOperandList()) {
            expr.accept(this);
        }
        if (o.getLastOperand() != null) {
            o.getLastOperand().accept(this);
        }

        return null;
    }

    public T3 visit(DPropertyTransform o) {
        if (o.getProperty() != null) {
            o.getProperty().accept(this);
        }

        return null;
    }

    public T3 visit(DPropertyRestriction o) {
        if (o.getProperty() != null) {
            o.getProperty().accept(this);
        }
        if (o.getDomain() != null) {
            o.getDomain().accept(this);
        }
        if (o.getRange() != null) {
            o.getRange().accept(this);
        }

        return null;
    }

    public T3 visit(InstanceUri o) {
        return null;
    }

    public T3 visit(Datatype o) {
        if (o.getRestriction() != null) {
            o.getRestriction().accept(this, null);
        }

        return null;
    }

    public T4 visit(BasicCondition c, T5 o) {
        return null;
    }

    public T4 visit(AndCondition c, T5 o) {
        for (Condition condition : c.getOperandList()) {
            condition.accept(this, o);
        }

        return null;
    }

    public T4 visit(OrCondition c, T5 o) {
        for (Condition condition : c.getOperandList()) {
            condition.accept(this, o);
        }

        return null;
    }

    public T4 visit(NotCondition c, T5 o) {
        if (c.getOperand() != null) {
            c.getOperand().accept(this, o);
        }

        return null;
    }

    public T6 visit(BinaryPredicate c, T7 o) {
        return null;
    }

    public T6 visit(UnaryPredicate c, T7 o) {
        return null;
    }

    public T6 visit(DataValue c, T7 o) {
        return null;
    }

    public T6 visit(InstanceUri c, T7 o) {
        return null;
    }
}
