/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.concept;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * An intersection of class expressions.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class ClassIntersection extends ClassExpression {

    private final List<ClassExpression> operandList;

    /**
     * Creates a ClassIntersection object using specified values.
     *
     * @param operandList A list of class expressions.
     */
    public ClassIntersection(List<ClassExpression> operandList) {
        this.operandList = new ArrayList<ClassExpression>(operandList);
    }

    public List<ClassExpression> getOperandList() {
        return operandList;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        for (ClassExpression expr : operandList) {
            resourceList.addAll(expr.listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.operandList != null ? this.operandList.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClassIntersection other = (ClassIntersection) obj;
        if (this.operandList != other.operandList && (this.operandList == null || !this.operandList.equals(other.operandList))) {
            return false;
        }
        return true;
    }
}
