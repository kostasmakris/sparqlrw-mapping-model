/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.property.datatype;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExpression;
import gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype;
import java.util.ArrayList;
import java.util.List;

/**
 * A datatype property restricted on its domain/range values.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class DPropertyRestriction extends DPropertyExpression {

    private final DPropertyExpression property;
    private final ClassExpression domain;
    private final Datatype range;

    /**
     * Creates a DPropertyRestriction object using specified values.
     *
     * @param property A datatype property expression.
     * @param domain   A class expression specifying the domain restriction.
     * @param range    A datatype specifying the range restriction.
     */
    public DPropertyRestriction(DPropertyExpression property, ClassExpression domain, Datatype range) {
        this.property = property;
        this.domain = domain;
        this.range = range;
    }

    public DPropertyExpression getProperty() {
        return property;
    }

    public ClassExpression getDomain() {
        return domain;
    }

    public Datatype getRange() {
        return range;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        if (property != null) {
            resourceList.addAll(property.listResources());
        }

        if (domain != null) {
            resourceList.addAll(domain.listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.property != null ? this.property.hashCode() : 0);
        hash = 59 * hash + (this.domain != null ? this.domain.hashCode() : 0);
        hash = 59 * hash + (this.range != null ? this.range.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DPropertyRestriction other = (DPropertyRestriction) obj;
        if (this.property != other.property && (this.property == null || !this.property.equals(other.property))) {
            return false;
        }
        if (this.domain != other.domain && (this.domain == null || !this.domain.equals(other.domain))) {
            return false;
        }
        if (this.range != other.range && (this.range == null || !this.range.equals(other.range))) {
            return false;
        }
        return true;
    }
}
