/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import gr.tuc.music.sparqlrw.mapping.model.concept.ClassMapping;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceMapping;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyInverse;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectInverseOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.slf4j.LoggerFactory;

/**
 * Computes new mappings for unmapped terms, using an already specified mapping
 * set and a set of ontologies (global and local).
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class MappingReasoner {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MappingReasoner.class);

    private MappingModel model;
    private OWLReasoner reasoner;
    private OWLOntologyManager manager;
    private OWLDataFactory factory;

    /**
     * Computes new mappings for unmapped terms, using an already specified
     * mapping set and a set of ontologies (global and local).
     *
     * @param model A SPARQL-RW mapping model.
     * @return A list of inferred mappings.
     */
    public List<Mapping> computeInfMappings(MappingModel model) {
        List<Mapping> result = new ArrayList<Mapping>();

        if ((model == null) || !model.isConsistent()) {
            log.warn("Input parameter is null or inconsistent.");
            return result;
        }

        this.model = model;

        // load all the referenced ontologies into a single ontology schema
        manager = OWLManager.createOWLOntologyManager();
        factory = manager.getOWLDataFactory();

        OWLOntology total;

        try {
            total = manager.createOntology();
        } catch (OWLOntologyCreationException ex) {
            log.debug("Reasoner validation check failed to be performed.", ex);
            return result;
        }

        manager.addAxioms(total, model.getGlobalOntology().getSchema().getAxioms());
        for (Ontology ontology : model.getLocalOntologyList()) {
            manager.addAxioms(total, ontology.getSchema().getAxioms());
        }

        // convert the mappings to an ontology schema and add them to the
        // previously constructed ontology
        OWLConverter converter = new OWLConverter();
        manager.addAxioms(total, converter.convert(model).getAxioms());

        // create a reasoner
        OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
        reasoner = reasonerFactory.createNonBufferingReasoner(total);

        // compute inferences
        reasoner.precomputeInferences();

        return getInfMappings();
    }

    /**
     * Fetches new mappings for unmapped resources (one mapping for each
     * resource).
     *
     * @return A list of mappings.
     */
    private List<Mapping> getInfMappings() {
        List<Mapping> mappingList = new ArrayList<Mapping>();

        // list the unmapped resources
        List<Resource> resourceList = model.listUnmappedResources();
        for (Resource resource : resourceList) {
            Mapping mapping;

            if (resource instanceof ClassUri) {
                // get a class mapping
                mapping = getClassMapping((ClassUri) resource);
            } else if (resource instanceof OPropertyUri) {
                // get an object property mapping
                mapping = getOPropertyMapping((OPropertyUri) resource);
            } else if (resource instanceof DPropertyUri) {
                // get a datatype property mapping
                mapping = getDPropertyMapping((DPropertyUri) resource);
            } else {
                // get an instance mapping
                mapping = getInstanceMapping((InstanceUri) resource);
            }

            if (mapping != null) {
                mappingList.add(mapping);
            }
        }

        return mappingList;
    }

    /**
     * Fetches a new mapping for a simple class (mappings involving equivalence
     * are given higher priority compared to mappings involving subsumption).
     *
     * @param expr1 A class.
     * @return A class mapping.
     */
    private ClassMapping getClassMapping(ClassUri expr1) {
        ClassMapping mapping = getClassMapping(expr1, Relation.EQUIVALENT);
        if (mapping != null) {
            return mapping;
        }

        mapping = getClassMapping(expr1, Relation.SUBSUMED);
        if (mapping != null) {
            return mapping;
        }

        mapping = getClassMapping(expr1, Relation.SUBSUMES);

        return mapping;
    }

    /**
     * Fetches a new mapping for a simple class based on a specified mapping
     * type.
     *
     * @param expr1    A class.
     * @param relation The mapping type (equivalence, subsumption).
     * @return A class mapping.
     */
    private ClassMapping getClassMapping(ClassUri expr1, Relation relation) {
        // get the input resource IRI
        IRI iri = IRI.create(expr1.getUri());

        // get an OWLClass representation for the input resource
        OWLClass clazz = factory.getOWLClass(iri);

        Set<OWLClass> classSet;
        if (relation == Relation.EQUIVALENT) {
            Node<OWLClass> node = reasoner.getEquivalentClasses(clazz);
            classSet = node.getEntities();
        } else if (relation == Relation.SUBSUMED) {
            NodeSet<OWLClass> nodeSet = reasoner.getSuperClasses(clazz, false);
            classSet = nodeSet.getFlattened();
        } else {
            NodeSet<OWLClass> nodeSet = reasoner.getSubClasses(clazz, false);
            classSet = nodeSet.getFlattened();
        }

        for (OWLClass resource : classSet) {
            ClassUri expr2 = new ClassUri(resource.getIRI().toString());
            String mappingUri = IRI.generateDocumentIRI().toString();
            ClassMapping mapping = new ClassMapping(mappingUri, expr1, expr2, relation);

            if (!model.violatesFormalism(mapping)) {
                return mapping;
            }
        }

        return null;
    }

    /**
     * Fetches a new mapping for a simple object property (mappings involving
     * equivalence are given higher priority compared to mappings involving
     * subsumption).
     *
     * @param expr1 An object property.
     * @return An object property mapping.
     */
    private OPropertyMapping getOPropertyMapping(OPropertyUri expr1) {
        OPropertyMapping mapping = getOPropertyMapping(expr1, Relation.EQUIVALENT);
        if (mapping != null) {
            return mapping;
        }

        mapping = getOPropertyMapping(expr1, Relation.SUBSUMED);
        if (mapping != null) {
            return mapping;
        }

        mapping = getOPropertyMapping(expr1, Relation.SUBSUMES);

        return mapping;
    }

    /**
     * Fetches a new mapping for a simple object property based on a specified
     * mapping type.
     *
     * @param expr1    An object property.
     * @param relation The mapping type (equivalence, subsumption).
     * @return An object property mapping.
     */
    private OPropertyMapping getOPropertyMapping(OPropertyUri expr1, Relation relation) {
        // get the input resource IRI
        IRI iri = IRI.create(expr1.getUri());

        // get an OWLObjectProperty representation for the input resource
        OWLObjectProperty property = factory.getOWLObjectProperty(iri);

        Set<OWLObjectPropertyExpression> propertySet;
        if (relation == Relation.EQUIVALENT) {
            Node<OWLObjectPropertyExpression> node = reasoner.getEquivalentObjectProperties(property);
            propertySet = node.getEntities();
        } else if (relation == Relation.SUBSUMED) {
            NodeSet<OWLObjectPropertyExpression> nodeSet = reasoner.getSuperObjectProperties(property, false);
            propertySet = nodeSet.getFlattened();
        } else {
            NodeSet<OWLObjectPropertyExpression> nodeSet = reasoner.getSubObjectProperties(property, false);
            propertySet = nodeSet.getFlattened();
        }

        for (OWLObjectPropertyExpression resource : propertySet) {
            OPropertyExpression expr2;

            // according to OWL-API javadoc the OWLObjectPropertyExpression
            // returned from functions like getSubObjectProperties can be either
            // an OWLObjectProperty or an OWLObjectInverseOf

            if (resource instanceof OWLObjectProperty) {
                expr2 = new OPropertyUri(((OWLObjectProperty) resource).getIRI().toString());
            } else {
                OWLObjectPropertyExpression expr = ((OWLObjectInverseOf) resource).getInverse();
                expr2 = new OPropertyInverse(new OPropertyUri(((OWLObjectProperty) expr).getIRI().toString()));
            }

            String mappingUri = IRI.generateDocumentIRI().toString();
            OPropertyMapping mapping = new OPropertyMapping(mappingUri, expr1, expr2, relation);

            if (!model.violatesFormalism(mapping)) {
                return mapping;
            }
        }

        return null;
    }

    /**
     * Fetches a new mapping for a simple datatype property (mappings involving
     * equivalence are given higher priority compared to mappings involving
     * subsumption).
     *
     * @param expr1 A datatype property.
     * @return A datatype property mapping.
     */
    private DPropertyMapping getDPropertyMapping(DPropertyUri expr1) {
        DPropertyMapping mapping = getDPropertyMapping(expr1, Relation.EQUIVALENT);
        if (mapping != null) {
            return mapping;
        }

        mapping = getDPropertyMapping(expr1, Relation.SUBSUMED);
        if (mapping != null) {
            return mapping;
        }

        mapping = getDPropertyMapping(expr1, Relation.SUBSUMES);

        return mapping;
    }

    /**
     * Fetches a new mapping for a simple datatype property based on a specified
     * mapping type.
     *
     * @param expr1    A datatype property.
     * @param relation The mapping type (equivalence, subsumption).
     * @return A datatype property mapping.
     */
    private DPropertyMapping getDPropertyMapping(DPropertyUri expr1, Relation relation) {
        // get the input resource IRI
        IRI iri = IRI.create(expr1.getUri());

        // get an OWLDataProperty representation for the input resource
        OWLDataProperty property = factory.getOWLDataProperty(iri);

        Set<OWLDataProperty> propertySet;
        if (relation == Relation.EQUIVALENT) {
            Node<OWLDataProperty> node = reasoner.getEquivalentDataProperties(property);
            propertySet = node.getEntities();
        } else if (relation == Relation.SUBSUMED) {
            NodeSet<OWLDataProperty> nodeSet = reasoner.getSuperDataProperties(property, false);
            propertySet = nodeSet.getFlattened();
        } else {
            NodeSet<OWLDataProperty> nodeSet = reasoner.getSubDataProperties(property, false);
            propertySet = nodeSet.getFlattened();
        }

        for (OWLDataProperty resource : propertySet) {
            DPropertyUri expr2 = new DPropertyUri(resource.getIRI().toString());
            String mappingUri = IRI.generateDocumentIRI().toString();
            DPropertyMapping mapping = new DPropertyMapping(mappingUri, expr1, expr2, relation);

            if (!model.violatesFormalism(mapping)) {
                return mapping;
            }
        }

        return null;
    }

    /**
     * Fetches a new mapping for a simple class instance.
     *
     * @param expr1 A class instance.
     * @return An instance mapping.
     */
    private InstanceMapping getInstanceMapping(InstanceUri expr1) {
        // get the input resource IRI
        IRI iri = IRI.create(expr1.getUri());

        // get an OWLNamedIndividual representation for the input resource
        OWLNamedIndividual owlIndividual = factory.getOWLNamedIndividual(iri);

        Node<OWLNamedIndividual> node = reasoner.getSameIndividuals(owlIndividual);
        Set<OWLNamedIndividual> owlIndividualSet = node.getEntities();

        for (OWLNamedIndividual resource : owlIndividualSet) {
            InstanceUri expr2 = new InstanceUri(resource.getIRI().toString());
            String mappingUri = IRI.generateDocumentIRI().toString();
            InstanceMapping mapping = new InstanceMapping(mappingUri, expr1, expr2, Relation.EQUIVALENT);

            if (!model.violatesFormalism(mapping)) {
                return mapping;
            }
        }

        return null;
    }
}
