/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.property.object;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * An inverse object property expression.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class OPropertyInverse extends OPropertyExpression {

    private final OPropertyExpression operand;

    /**
     * Creates an OPropertyInverse object using specified values.
     *
     * @param operand An object property expression.
     */
    public OPropertyInverse(OPropertyExpression operand) {
        this.operand = operand;
    }

    public OPropertyExpression getOperand() {
        return operand;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        if (operand != null) {
            resourceList.addAll(operand.listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.operand != null ? this.operand.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OPropertyInverse other = (OPropertyInverse) obj;
        if (this.operand != other.operand && (this.operand == null || !this.operand.equals(other.operand))) {
            return false;
        }
        return true;
    }
}
