/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.slf4j.LoggerFactory;

/**
 * The Mapping Model of the SPARQL-RW Framework.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class MappingModel {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(MappingModel.class);

    private final Formalism formalism;
    private final Ontology globalOntology;
    private final List<Ontology> localOntologyList;
    private final List<Mapping> mappingList;
    private final HashMap<String, Mapping> mappingUriMap;
    private final HashMap<Expression, Mapping> mappingExprMap;

    /**
     * Creates a MappingModel object using specified values.
     *
     * @param formalism         The adopted mapping formalism.
     * @param globalOntology    The global ontology.
     * @param localOntologyList The local ontology list.
     * @param mappingList       The mapping list.
     */
    public MappingModel(Formalism formalism, Ontology globalOntology, List<Ontology> localOntologyList, List<Mapping> mappingList) {
        this.formalism = formalism;
        this.globalOntology = globalOntology;
        this.localOntologyList = new ArrayList<Ontology>(localOntologyList);
        this.mappingList = new ArrayList<Mapping>();

        mappingUriMap = new HashMap<String, Mapping>();
        mappingExprMap = new HashMap<Expression, Mapping>();

        addMappingList(mappingList);
    }

    public Formalism getFormalism() {
        return formalism;
    }

    public Ontology getGlobalOntology() {
        return globalOntology;
    }

    public List<Ontology> getLocalOntologyList() {
        return localOntologyList;
    }

    public List<Mapping> getMappingList() {
        return mappingList;
    }

    public void clearMappingList() {
        mappingList.clear();
        mappingUriMap.clear();
        mappingExprMap.clear();
    }

    public void setMappingList(List<Mapping> mappingList) {
        clearMappingList();
        addMappingList(mappingList);
    }

    public final void addMappingList(List<Mapping> mappingList) {
        this.mappingList.addAll(mappingList);

        for (Mapping mapping : this.mappingList) {
            String uri = null;
            Expression expr1 = mapping.getExpr1();

            if (expr1 instanceof ClassUri) {
                ClassUri classUri = (ClassUri) expr1;
                uri = classUri.getUri();
            } else if (expr1 instanceof OPropertyUri) {
                OPropertyUri oPropertyUri = (OPropertyUri) expr1;
                uri = oPropertyUri.getUri();
            } else if (expr1 instanceof DPropertyUri) {
                DPropertyUri dPropertyUri = (DPropertyUri) expr1;
                uri = dPropertyUri.getUri();
            } else if (expr1 instanceof InstanceUri) {
                InstanceUri instanceUri = (InstanceUri) expr1;
                uri = instanceUri.getUri();
            }

            if (uri != null) {
                mappingUriMap.put(uri, mapping);
            }

            mappingExprMap.put(expr1, mapping);
        }
    }

    public void addMapping(Mapping mapping) {
        if (mapping != null) {
            List<Mapping> mappingList = new ArrayList<Mapping>();
            mappingList.add(mapping);
            addMappingList(mappingList);
        }
    }

    /**
     * Fetches a mapping (if any) for a specified ontology term.
     *
     * @param uri An ontology term uri.
     * @return A mapping for the specified expression.
     */
    public Mapping getMapping(String uri) {

        if (uri == null) {
            return null;
        }

        return mappingUriMap.get(uri);
    }

    /**
     * Fetches a mapping (if any) for a specified expression.
     *
     * @param expr An expression.
     * @return A mapping for the specified expression.
     */
    public Mapping getMapping(Expression expr) {

        if (expr == null) {
            return null;
        }

        return mappingExprMap.get(expr);
    }

    /**
     * Checks whether or not the mapping model contains a mapping for a given
     * ontology term.
     *
     * @param uri An ontology term uri.
     * @return True in case the mapping model contains a mapping for the input
     *         ontology term, otherwise false.
     */
    public boolean hasMapping(String uri) {

        if (uri == null) {
            return false;
        }

        return mappingUriMap.containsKey(uri);
    }

    /**
     * Resolves the ontology in which a given resource is specified.
     *
     * @param uri An ontology term uri.
     * @return An ontology uri.
     */
    public String resolveOntology(String uri) {

        if (uri == null) {
            return null;
        }

        for (Ontology ontology : localOntologyList) {
            if (ontology.contains(uri)) {
                return ontology.getUri();
            }
        }

        return null;
    }

    /**
     * Checks if a mapping is candidate to be added to the mapping model, based
     * on: (a) the fact that there is no other mapping for its left side
     * expression, (b) the mapping model formalism and (c) the ontology
     * (global/local) where the expression resources have been specified).
     *
     * @param mapping A mapping.
     * @return True if the mapping is candidate to be added to a mapping model,
     *         otherwise false.
     */
    public boolean isCandidate(Mapping mapping) {
        boolean result = true;

        // check if the mapping contains inconsistent resources (a class, property or
        // individual is specified differently in its corresponding ontology schema)
        if (containsInconsistentResources(mapping)) {
            log.warn("Mapping contains inconsistent resources.");
            result = false;
        }

        // check if the mapping is already specified
        if (mappingExprMap.containsKey(mapping.getExpr1())) {
            log.warn("Mapping is already specified.");
            result = false;
        }

        // check if the mapping violates the specified formalism
        if (violatesFormalism(mapping)) {
            log.warn("Mapping violates the specified formalism.");
            result = false;
        }

        return result;
    }

    /**
     * Checks if the mapping model is consistent.
     *
     * @return True if the mapping model is consistent, otherwise false.
     */
    public boolean isConsistent() {
        boolean result = true;

        // check if the referenced ontology schemas have been loaded
        if (containsMissingOntSchemas()) {
            log.warn("Mapping model references missing ontology schemas.");
            return false;
        }

        // check if the mappings contain inconsistent resources (a class, property or
        // individual is specified differently in its corresponding ontology schema)
        if (containsInconsistentResources()) {
            log.warn("Mapping model contains mappings with inconsistent resources.");
            result = false;
        }

        // check if the mapping model contains duplicate mapping definitions
        if (containsMappingDuplicates()) {
            log.warn("Mapping model contains mapping duplicates.");
            result = false;
        }

        // check if the mappings violate the specified formalism
        if (violatesFormalism()) {
            log.warn("Mapping model violates the specified formalism.");
            result = false;
        }

        // check for validation using a reasoner
        if (failsReasonerValidation()) {
            log.warn("Mapping model fails reasoner validation.");
            result = false;
        }

        return result;
    }

    /**
     * Lists the resources of the global/local ontology (depending on the
     * GAV\LAV formalism) that have not been mapped.
     *
     * @return A list of unmapped resources.
     */
    public List<Resource> listUnmappedResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        if (formalism == Formalism.GAV) {
            // add the global ontology resources to the list
            resourceList.addAll(globalOntology.listResources());

            // remove the resources identified in the left part of the mappings
            for (Mapping mapping : mappingList) {
                resourceList.removeAll(mapping.getExpr1().listResources());
            }
        } else if (formalism == Formalism.LAV) {
            // add the local ontology resources to the list
            for (Ontology ontology : localOntologyList) {
                resourceList.addAll(ontology.listResources());
            }

            // remove the resources identified in the left part of the mappings
            for (Mapping mapping : mappingList) {
                resourceList.removeAll(mapping.getExpr1().listResources());
            }
        }

        return resourceList;
    }

    /**
     * Checks if the mapping model misses some referenced ontology schemas.
     *
     * @return True if the mapping model misses some referenced ontology
     *         schemas, otherwise false.
     */
    public boolean containsMissingOntSchemas() {
        boolean result = false;

        if ((globalOntology == null) || (globalOntology.getSchema() == null)) {
            log.info("Global ontology schema has not been loaded.");
            result = true;
        }

        for (Ontology ontology : localOntologyList) {
            if (ontology.getSchema() == null) {
                log.info("Local ontology <" + ontology.getUri() + "> schema has not been loaded.");
                result = true;
            }
        }

        return result;
    }

    /**
     * Checks if the mapping model contains inconsistent resources (i.e., a
     * class, property or instance is specified differently in the mapping, and
     * in its corresponding ontology schema).
     *
     * @return True if the mapping model contains inconsistent resources,
     *         otherwise false.
     */
    public boolean containsInconsistentResources() {
        boolean result = false;

        for (Mapping mapping : mappingList) {
            if (containsInconsistentResources(mapping)) {
                result = true;
            }
        }

        return result;
    }

    /**
     * Checks if a specified mapping contains inconsistent resources (i.e., a
     * class, property or instance is specified differently in the mapping, and
     * in its corresponding ontology schema).
     *
     * @param mapping A mapping.
     * @return True if the mapping contains inconsistent resources, otherwise
     *         false.
     */
    public boolean containsInconsistentResources(Mapping mapping) {
        boolean result = false;

        List<Resource> resourceList = new ArrayList<Resource>(mapping.getExpr1().listResources());
        resourceList.addAll(mapping.getExpr2().listResources());

        for (Resource resource : resourceList) {
            Ontology ontology = resolveOntology(resource);

            if (ontology == null) {
                log.info("[Mapping: <" + mapping.getUri() + ">] resource: <" + resource.getUri() + "> is not specified in any ontology.");
                result = true;
            } else {
                if ((resource instanceof ClassUri) && !ontology.containsClass(resource)) {
                    log.info("[Mapping: <" + mapping.getUri() + ">] resource: <" + resource.getUri() + "> is handled as class but is not specified as such in its correspoding ontology.");
                    result = true;
                } else if ((resource instanceof OPropertyUri) && !ontology.containsOProperty(resource)) {
                    log.info("[Mapping: <" + mapping.getUri() + ">] resource: <" + resource.getUri() + "> is handled as object property but is not specified as such in its correspoding ontology.");
                    result = true;
                } else if ((resource instanceof DPropertyUri) && !ontology.containsDProperty(resource)) {
                    log.info("[Mapping: <" + mapping.getUri() + ">] resource: <" + resource.getUri() + "> is handled as datatype property but is not specified as such in its correspoding ontology.");
                    result = true;
                } else if ((resource instanceof InstanceUri) && !ontology.containsInstance(resource)) {
                    log.info("[Mapping: <" + mapping.getUri() + ">] resource: <" + resource.getUri() + "> is handled as class instance but is not specified as such in its correspoding ontology.");
                    result = true;
                }
            }
        }

        return result;
    }

    /**
     * Checks if the mapping model contains mapping duplicates (multiple
     * mappings for a specific expression).
     *
     * @return True if the mapping model contains mapping duplicates, otherwise
     *         false.
     */
    public boolean containsMappingDuplicates() {
        boolean result = false;

        HashMap<Expression, Mapping> mappingHashMap = new HashMap<Expression, Mapping>();
        for (Mapping mapping : mappingList) {
            if (!mappingHashMap.containsKey(mapping.getExpr1())) {
                mappingHashMap.put(mapping.getExpr1(), mapping);
            } else {
                log.info("[Mapping: <" + mapping.getUri() + ">] is already specified.");
                result = true;
            }
        }

        return result;
    }

    /**
     * Checks if the mapping model violates its specified formalism.
     *
     * @return True if the mapping model violates its specified formalism,
     *         otherwise false.
     */
    public boolean violatesFormalism() {
        boolean result = false;

        for (Mapping mapping : mappingList) {
            if (violatesFormalism(mapping)) {
                result = true;
            }
        }

        return result;
    }

    /**
     * Checks if a specified mapping violates the mapping model's formalism.
     *
     * @param mapping A mapping.
     * @return True if the mapping violates the mapping model's formalism,
     *         otherwise false.
     */
    public boolean violatesFormalism(Mapping mapping) {
        boolean result = false;

        if ((formalism == Formalism.GAV) && !isGAV(mapping)) {
            log.info("[Mapping: <" + mapping.getUri() + ">] violates the GAV formalism.");
            result = true;
        } else if ((formalism == Formalism.LAV) && !isLAV(mapping)) {
            log.info("[Mapping: <" + mapping.getUri() + ">] violates the LAV formalism.");
            result = true;
        } else if ((formalism == Formalism.GLAV) && !isGLAV(mapping)) {
            log.info("[Mapping: <" + mapping.getUri() + ">] violates the GLAV formalism.");
            result = true;
        }

        return result;
    }

    public <T> T accept(MappingModelVisitor<T> visitor) {
        return visitor.visit(this);
    }

    /**
     * Checks if the mapping model fails the reasoner validation.
     *
     * @return True if the mapping model fails the reasoner validation,
     *         otherwise false.
     */
    private boolean failsReasonerValidation() {
        OWLConverter converter = new OWLConverter();

        // load all the referenced ontologies into a single ontology schema
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology total;

        try {
            total = manager.createOntology();
        } catch (OWLOntologyCreationException ex) {
            log.debug("Reasoner validation check failed to be performed.", ex);
            return true;
        }

        manager.addAxioms(total, globalOntology.getSchema().getAxioms());
        for (Ontology ontology : localOntologyList) {
            manager.addAxioms(total, ontology.getSchema().getAxioms());
        }

        // convert the mappings to an ontology schema and add them to the
        // previously constructed ontology
        manager.addAxioms(total, converter.convert(this).getAxioms());

        // create a reasoner
        OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();

        // create a reasoner that will reason over our ontology and its imports closure
        OWLReasoner reasoner = reasonerFactory.createReasoner(total);

        // ask the reasoner to do all the necessary work now
        reasoner.precomputeInferences();

        // we can determine if the ontology is actually consistent
        boolean consistent = reasoner.isConsistent();

        return !consistent;
    }

    /**
     * Resolves the ontology that references a given resource.
     *
     * @param resource A resource.
     * @return The ontology that references the given resource.
     */
    private Ontology resolveOntology(Resource resource) {
        Ontology result = null;

        if (resource == null) {
            return result;
        }

        // check if the resource is specified in the global ontology
        if (globalOntology.contains(resource)) {
            result = globalOntology;
            return result;
        }

        // check if the resource is specified in one of the local ontologies
        for (Ontology ontology : localOntologyList) {
            if (ontology.contains(resource)) {
                result = ontology;
                break;
            }
        }

        return result;
    }

    /**
     * Checks if a resource is specified in the global ontology.
     *
     * @param resource A resource.
     * @return True if the resource is specified in the global ontology,
     *         otherwise false.
     */
    private boolean isGlobal(Resource resource) {
        boolean result = true;

        if (!globalOntology.contains(resource)) {
            return false;
        }

        return result;
    }

    /**
     * Checks if a resource list contains resources that are only specified in
     * the global ontology.
     *
     * @param resourceList A list of resources.
     * @return True if the resource list contains resources that are only
     *         specified in the global ontology, otherwise false.
     */
    private boolean areGlobal(List<Resource> resourceList) {
        boolean result = true;

        for (Resource resource : resourceList) {
            if (!globalOntology.contains(resource)) {
                return false;
            }
        }

        return result;
    }

    /**
     * Checks if a resource is specified in the local ontologies.
     *
     * @param resource A resource.
     * @return True if the resource is specified in the local ontologies,
     *         otherwise false.
     */
    private boolean isLocal(Resource resource) {
        boolean result = true;

        for (Ontology ontology : localOntologyList) {
            if (ontology.contains(resource)) {
                break;
            }
        }

        return result;
    }

    /**
     * Checks if a resource list contains resources that are only specified in
     * the local ontologies.
     *
     * @param resourceList A list of resources.
     * @return True if the resource list contains resources that are only
     *         specified in the local ontologies, otherwise false.
     */
    private boolean areLocal(List<Resource> resourceList) {
        boolean result = true;

        for (Resource resource : resourceList) {
            boolean flag = false;

            for (Ontology ontology : localOntologyList) {
                if (ontology.contains(resource)) {
                    flag = true;
                    break;
                }
            }

            if (flag == false) {
                return false;
            }
        }

        return result;
    }

    /**
     * Checks if a mapping obeys the GAV formalism, considering the mapping
     * model's global and local ontologies.
     *
     * @param mapping A mapping.
     * @return True if the mapping obeys the GAV formalism, otherwise false.
     */
    private boolean isGAV(Mapping mapping) {
        boolean result = true;

        if ((mapping == null) || ((mapping.getExpr1() == null)) || (mapping.getExpr2() == null)) {
            log.debug("Input parameter is empty or null.");
            return false;
        }

        List<Resource> expr1ResourceList = mapping.getExpr1().listResources();
        List<Resource> expr2ResourceList = mapping.getExpr2().listResources();

        if (expr1ResourceList.size() > 1) {
            log.info("[Mapping: <" + mapping.getUri() + ">] contains complex expression on the left.");
            result = false;
        }

        if (!areGlobal(expr1ResourceList)) {
            log.info("[Mapping: <" + mapping.getUri() + ">] contains non global ontology terms on the left.");
            result = false;
        }

        if (!areLocal(expr2ResourceList)) {
            log.info("[Mapping: <" + mapping.getUri() + ">] contains non local ontology terms on the right.");
            result = false;
        }

        return result;
    }

    /**
     * Checks if a mapping obeys the LAV formalism, considering the mapping
     * model's global and local ontologies.
     *
     * @param mapping A mapping.
     * @return True if the mapping obeys the LAV formalism, otherwise false.
     */
    private boolean isLAV(Mapping mapping) {
        boolean result = true;

        if ((mapping == null) || ((mapping.getExpr1() == null)) || (mapping.getExpr2() == null)) {
            log.debug("Input parameter is empty or null.");
            return false;
        }

        List<Resource> expr1ResourceList = mapping.getExpr1().listResources();
        List<Resource> expr2ResourceList = mapping.getExpr2().listResources();

        if (expr1ResourceList.size() > 1) {
            log.info("[Mapping: <" + mapping.getUri() + ">] contains complex expression on the left.");
            result = false;
        }

        if (!areLocal(expr1ResourceList)) {
            log.info("[Mapping: <" + mapping.getUri() + ">] contains non local ontology terms on the left.");
            result = false;
        }

        if (!areGlobal(expr2ResourceList)) {
            log.info("[Mapping: <" + mapping.getUri() + ">] contains non global ontology terms on the right.");
            result = false;
        }

        return result;
    }

    /**
     * Checks if a mapping obeys the GLAV formalism, considering the mapping
     * model's global and local ontologies.
     *
     * @param mapping A mapping.
     * @return True if the mapping obeys the GLAV formalism, otherwise false.
     */
    private boolean isGLAV(Mapping mapping) {
        boolean result = true;

        if ((mapping == null) || ((mapping.getExpr1() == null)) || (mapping.getExpr2() == null)) {
            log.debug("Input parameter is empty or null.");
            return false;
        }

        List<Resource> expr1ResourceList = mapping.getExpr1().listResources();
        List<Resource> expr2ResourceList = mapping.getExpr2().listResources();

        if (!areLocal(expr1ResourceList)) {
            log.info("[Mapping: <" + mapping.getUri() + ">] contains non local ontology terms on the left.");
            result = false;
        }

        if (!areGlobal(expr2ResourceList)) {
            log.info("[Mapping: <" + mapping.getUri() + ">] contains non global ontology terms on the right.");
            result = false;
        }

        return result;
    }
}
