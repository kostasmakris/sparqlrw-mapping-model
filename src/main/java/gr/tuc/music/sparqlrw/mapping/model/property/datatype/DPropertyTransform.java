/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.property.datatype;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import gr.tuc.music.sparqlrw.mapping.model.datarange.XmlDatatype;
import java.util.ArrayList;
import java.util.List;


/**
 * A datatype property having its range values transformed.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class DPropertyTransform extends DPropertyExpression {

    private final DPropertyExpression property;
    private final XmlDatatype datatype;

    /**
     * Creates a DPropertyTransform object using specified values.
     *
     * @param property A datatype property expression.
     * @param datatype A datatype specifying the value transformation.
     */
    public DPropertyTransform(DPropertyExpression property, XmlDatatype datatype) {
        this.property = property;
        this.datatype = datatype;
    }

    public DPropertyExpression getProperty() {
        return property;
    }

    public XmlDatatype getDatatype() {
        return datatype;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        if (property != null) {
            resourceList.addAll(property.listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.property != null ? this.property.hashCode() : 0);
        hash = 29 * hash + (this.datatype != null ? this.datatype.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DPropertyTransform other = (DPropertyTransform) obj;
        if (this.property != other.property && (this.property == null || !this.property.equals(other.property))) {
            return false;
        }
        if (this.datatype != other.datatype) {
            return false;
        }
        return true;
    }
}
