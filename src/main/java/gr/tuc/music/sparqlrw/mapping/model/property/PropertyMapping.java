/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.property;

import gr.tuc.music.sparqlrw.mapping.model.Mapping;
import gr.tuc.music.sparqlrw.mapping.model.MappingVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Relation;

/**
 * A mapping between property expressions.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public abstract class PropertyMapping extends Mapping {

    /**
     * Creates a PropertyMapping object using specified values.
     *
     * @param uri      The mapping uri.
     * @param expr1    The left side property expression.
     * @param expr2    The right side property expression.
     * @param relation The mapping relation.
     */
    public PropertyMapping(String uri, PropertyExpression expr1, PropertyExpression expr2, Relation relation) {
        super(uri, expr1, expr2, relation);
    }

    @Override
    public PropertyExpression getExpr1() {
        return (PropertyExpression) super.getExpr1();
    }

    @Override
    public PropertyExpression getExpr2() {
        return (PropertyExpression) super.getExpr2();
    }

    @Override
    public abstract <T> T accept(MappingVisitor<T> visitor);
}
