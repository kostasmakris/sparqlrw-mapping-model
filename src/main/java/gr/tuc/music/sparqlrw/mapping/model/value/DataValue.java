/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.value;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import gr.tuc.music.sparqlrw.mapping.model.datarange.XmlDatatype;
import java.util.ArrayList;
import java.util.List;

/**
 * A data value.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class DataValue implements Value {

    private final String value;
    private final XmlDatatype datatype;

    /**
     * Creates a DataValue object using specified values.
     *
     * @param value    A data value.
     * @param datatype A datatype.
     */
    public DataValue(String value, XmlDatatype datatype) {
        this.value = value;
        this.datatype = datatype;
    }

    public String getValue() {
        return value;
    }

    public XmlDatatype getDatatype() {
        return datatype;
    }

    public List<Resource> listResources() {
        return new ArrayList<Resource>();
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return null;
    }

    @Override
    public <T> T accept(ValueVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.value != null ? this.value.hashCode() : 0);
        hash = 67 * hash + (this.datatype != null ? this.datatype.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataValue other = (DataValue) obj;
        if ((this.value == null) ? (other.value != null) : !this.value.equals(other.value)) {
            return false;
        }
        if (this.datatype != other.datatype) {
            return false;
        }
        return true;
    }
}
