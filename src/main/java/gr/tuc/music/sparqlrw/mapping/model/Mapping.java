/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

/**
 * A mapping between expressions.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public abstract class Mapping {

    private final String uri;
    private final Expression expr1;
    private final Expression expr2;
    private final Relation relation;

    /**
     * Creates a Mapping object using specified values.
     *
     * @param uri      The mapping uri.
     * @param expr1    The left side expression.
     * @param expr2    The right side expression.
     * @param relation The mapping relation.
     */
    public Mapping(String uri, Expression expr1, Expression expr2, Relation relation) {
        this.uri = uri;
        this.expr1 = expr1;
        this.expr2 = expr2;
        this.relation = relation;
    }

    public String getUri() {
        return uri;
    }

    public Expression getExpr1() {
        return expr1;
    }

    public Expression getExpr2() {
        return expr2;
    }

    public Relation getRelation() {
        return relation;
    }

    public abstract <T> T accept(MappingVisitor<T> visitor);
}
