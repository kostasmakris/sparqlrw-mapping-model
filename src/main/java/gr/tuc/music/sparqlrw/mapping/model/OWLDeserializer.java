/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAnnotationPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLAnonymousIndividual;
import org.semanticweb.owlapi.model.OWLAsymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataComplementOf;
import org.semanticweb.owlapi.model.OWLDataExactCardinality;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataIntersectionOf;
import org.semanticweb.owlapi.model.OWLDataMaxCardinality;
import org.semanticweb.owlapi.model.OWLDataMinCardinality;
import org.semanticweb.owlapi.model.OWLDataOneOf;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLDataUnionOf;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLDatatypeDefinitionAxiom;
import org.semanticweb.owlapi.model.OWLDatatypeRestriction;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLDifferentIndividualsAxiom;
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLFacetRestriction;
import org.semanticweb.owlapi.model.OWLFunctionalDataPropertyAxiom;
import org.semanticweb.owlapi.model.OWLFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLHasKeyAxiom;
import org.semanticweb.owlapi.model.OWLInverseFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLIrreflexiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLNegativeDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLNegativeObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectExactCardinality;
import org.semanticweb.owlapi.model.OWLObjectHasSelf;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectInverseOf;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLObjectMinCardinality;
import org.semanticweb.owlapi.model.OWLObjectOneOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLObjectVisitor;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLReflexiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLSameIndividualAxiom;
import org.semanticweb.owlapi.model.OWLSubAnnotationPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLSubDataPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom;
import org.semanticweb.owlapi.model.OWLSymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLTransitiveObjectPropertyAxiom;
import org.semanticweb.owlapi.model.SWRLBuiltInAtom;
import org.semanticweb.owlapi.model.SWRLClassAtom;
import org.semanticweb.owlapi.model.SWRLDataPropertyAtom;
import org.semanticweb.owlapi.model.SWRLDataRangeAtom;
import org.semanticweb.owlapi.model.SWRLDifferentIndividualsAtom;
import org.semanticweb.owlapi.model.SWRLIndividualArgument;
import org.semanticweb.owlapi.model.SWRLLiteralArgument;
import org.semanticweb.owlapi.model.SWRLObjectPropertyAtom;
import org.semanticweb.owlapi.model.SWRLRule;
import org.semanticweb.owlapi.model.SWRLSameIndividualAtom;
import org.semanticweb.owlapi.model.SWRLVariable;
import org.slf4j.LoggerFactory;

/**
 * Deserializes mappings represented in OWL2.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class OWLDeserializer implements OWLObjectVisitor {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(OWLDeserializer.class);

    private MappingModel model;
    private OWLOntologyManager manager;
    private OWLDataFactory factory;
    private OWLOntology ontology;

    List<Mapping> mappingList;

    /**
     * Deserializes an input OWL file and imports any identified mappings to a
     * specified SPARQL-RW mapping model.
     *
     * @param file  An OWL file containing mappings.
     * @param model The SPARQL-RW mapping model where the identified mappings
     *              have to be imported.
     */
    public void deserialize(File file, MappingModel model) {

        if ((model == null) || !model.isConsistent()) {
            log.warn("Input parameter is null or inconsistent.");
            return;
        }

        this.model = model;

        // load all the referenced ontologies into a single ontology schema
        manager = OWLManager.createOWLOntologyManager();
        factory = manager.getOWLDataFactory();

        try {
            ontology = manager.loadOntologyFromOntologyDocument(file);
        } catch (OWLOntologyCreationException ex) {
            log.debug("Ontology failed to be loaded.", ex);
            return;
        }

        mappingList = new ArrayList<Mapping>();

        // compute new mappings from the given OWL ontology
        computeMappings();

        // add the new mappings to the given mapping model
        model.getMappingList().addAll(mappingList);
    }

    public void visit(OWLDeclarationAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLSubClassOfAxiom axiom) {
        // TODO
    }

    public void visit(OWLNegativeObjectPropertyAssertionAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLAsymmetricObjectPropertyAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLReflexiveObjectPropertyAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDisjointClassesAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDataPropertyDomainAxiom axiom) {
        // TODO
    }

    public void visit(OWLObjectPropertyDomainAxiom axiom) {
        // TODO
    }

    public void visit(OWLEquivalentObjectPropertiesAxiom axiom) {
        // TODO
    }

    public void visit(OWLNegativeDataPropertyAssertionAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDifferentIndividualsAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDisjointDataPropertiesAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDisjointObjectPropertiesAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLObjectPropertyRangeAxiom axiom) {
        // TODO
    }

    public void visit(OWLObjectPropertyAssertionAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLFunctionalObjectPropertyAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLSubObjectPropertyOfAxiom axiom) {
        // TODO
    }

    public void visit(OWLDisjointUnionAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLSymmetricObjectPropertyAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDataPropertyRangeAxiom axiom) {
        // TODO
    }

    public void visit(OWLFunctionalDataPropertyAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLEquivalentDataPropertiesAxiom axiom) {
        // TODO
    }

    public void visit(OWLClassAssertionAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLEquivalentClassesAxiom axiom) {
        // TODO
    }

    public void visit(OWLDataPropertyAssertionAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLTransitiveObjectPropertyAxiom axiom) {
        // TODO
    }

    public void visit(OWLIrreflexiveObjectPropertyAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLSubDataPropertyOfAxiom axiom) {
        // TODO
    }

    public void visit(OWLInverseFunctionalObjectPropertyAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLSameIndividualAxiom axiom) {
        // TODO
    }

    public void visit(OWLSubPropertyChainOfAxiom axiom) {
        // TODO
    }

    public void visit(OWLInverseObjectPropertiesAxiom axiom) {
        // TODO
    }

    public void visit(OWLHasKeyAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDatatypeDefinitionAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLRule rule) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLAnnotationAssertionAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLSubAnnotationPropertyOfAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLAnnotationPropertyDomainAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLAnnotationPropertyRangeAxiom axiom) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLClass ce) {
        // TODO
    }

    public void visit(OWLObjectIntersectionOf ce) {
        // TODO
    }

    public void visit(OWLObjectUnionOf ce) {
        // TODO
    }

    public void visit(OWLObjectComplementOf ce) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLObjectSomeValuesFrom ce) {
        // TODO
    }

    public void visit(OWLObjectAllValuesFrom ce) {
        // TODO
    }

    public void visit(OWLObjectHasValue ce) {
        // TODO
    }

    public void visit(OWLObjectMinCardinality ce) {
        // TODO
    }

    public void visit(OWLObjectExactCardinality ce) {
        // TODO
    }

    public void visit(OWLObjectMaxCardinality ce) {
        // TODO
    }

    public void visit(OWLObjectHasSelf ce) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLObjectOneOf ce) {
        // TODO
    }

    public void visit(OWLDataSomeValuesFrom ce) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDataAllValuesFrom ce) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDataHasValue ce) {
        // TODO
    }

    public void visit(OWLDataMinCardinality ce) {
        // TODO
    }

    public void visit(OWLDataExactCardinality ce) {
        // TODO
    }

    public void visit(OWLDataMaxCardinality ce) {
        // TODO
    }

    public void visit(OWLLiteral node) {
        // TODO
    }

    public void visit(OWLFacetRestriction node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDatatype node) {
        // TODO
    }

    public void visit(OWLDataOneOf node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLDataComplementOf node) {
        // TODO
    }

    public void visit(OWLDataIntersectionOf node) {
        // TODO
    }

    public void visit(OWLDataUnionOf node) {
        // TODO
    }

    public void visit(OWLDatatypeRestriction node) {
        // TODO
    }

    public void visit(OWLObjectProperty property) {
        // TODO
    }

    public void visit(OWLObjectInverseOf property) {
        // TODO
    }

    public void visit(OWLDataProperty property) {
        // TODO
    }

    public void visit(OWLNamedIndividual individual) {
        // TODO
    }

    public void visit(OWLAnnotationProperty property) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLOntology ontology) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(OWLAnonymousIndividual individual) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(IRI iri) {
        // TODO
    }

    public void visit(OWLAnnotation node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLClassAtom node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLDataRangeAtom node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLObjectPropertyAtom node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLDataPropertyAtom node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLBuiltInAtom node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLVariable node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLIndividualArgument node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLLiteralArgument node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLSameIndividualAtom node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void visit(SWRLDifferentIndividualsAtom node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Computes new mappings for unmapped resources (one mapping for each
     * resource).
     *
     * @return A list of mappings.
     */
    private void computeMappings() {

        for (OWLEquivalentClassesAxiom axiom : ontology.getAxioms(AxiomType.EQUIVALENT_CLASSES)) {
            axiom.accept(this);
        }
        for (OWLSubClassOfAxiom axiom : ontology.getAxioms(AxiomType.SUBCLASS_OF)) {
            axiom.accept(this);
        }

        for (OWLEquivalentObjectPropertiesAxiom axiom : ontology.getAxioms(AxiomType.EQUIVALENT_OBJECT_PROPERTIES)) {
            axiom.accept(this);
        }
        for (OWLSubObjectPropertyOfAxiom axiom : ontology.getAxioms(AxiomType.SUB_OBJECT_PROPERTY)) {
            axiom.accept(this);
        }

        for (OWLEquivalentDataPropertiesAxiom axiom : ontology.getAxioms(AxiomType.EQUIVALENT_DATA_PROPERTIES)) {
            axiom.accept(this);
        }
        for (OWLSubDataPropertyOfAxiom axiom : ontology.getAxioms(AxiomType.SUB_DATA_PROPERTY)) {
            axiom.accept(this);
        }
    }
}
