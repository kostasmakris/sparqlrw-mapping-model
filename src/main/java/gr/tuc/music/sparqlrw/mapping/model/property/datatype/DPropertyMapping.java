/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.property.datatype;

import gr.tuc.music.sparqlrw.mapping.model.MappingVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Relation;
import gr.tuc.music.sparqlrw.mapping.model.property.PropertyMapping;

/**
 * A mapping between datatype property expressions.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class DPropertyMapping extends PropertyMapping {

    /**
     * Creates a DPropertyMapping object using specified values.
     *
     * @param uri      The mapping uri.
     * @param expr1    The left side datatype property expression.
     * @param expr2    The right side datatype property expression.
     * @param relation The mapping relation.
     */
    public DPropertyMapping(String uri, DPropertyExpression expr1, DPropertyExpression expr2, Relation relation) {
        super(uri, expr1, expr2, relation);
    }

    @Override
    public DPropertyExpression getExpr1() {
        return (DPropertyExpression) super.getExpr1();
    }

    @Override
    public DPropertyExpression getExpr2() {
        return (DPropertyExpression) super.getExpr2();
    }

    @Override
    public <T> T accept(MappingVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
