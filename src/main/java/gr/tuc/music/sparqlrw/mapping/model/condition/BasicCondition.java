/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.condition;

import gr.tuc.music.sparqlrw.mapping.model.predicate.UnaryPredicate;

/**
 * A basic value condition.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class BasicCondition extends Condition {

    private final UnaryPredicate restriction;

    /**
     * Creates a BasicCondition object using specified values.
     *
     * @param restriction A unary predicate restriction.
     */
    public BasicCondition(UnaryPredicate restriction) {
        this.restriction = restriction;
    }

    public UnaryPredicate getRestriction() {
        return restriction;
    }

    @Override
    public <T1, T2> T1 accept(ConditionVisitor<T1, T2> visitor, T2 o) {
        return visitor.visit(this, o);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.restriction != null ? this.restriction.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BasicCondition other = (BasicCondition) obj;
        if (this.restriction != other.restriction && (this.restriction == null || !this.restriction.equals(other.restriction))) {
            return false;
        }
        return true;
    }
}
