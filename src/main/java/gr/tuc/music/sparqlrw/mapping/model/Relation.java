/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

/**
 * An enumeration for the supported mapping relations (equivalent, subsumes ~
 * includes, subsumed ~ included by, unspecified).
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public enum Relation {

    EQUIVALENT, SUBSUMES, SUBSUMED, UNSPECIFIED;
}
