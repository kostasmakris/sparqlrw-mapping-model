/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.concept;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import gr.tuc.music.sparqlrw.mapping.model.property.PropertyExpression;
import java.util.ArrayList;
import java.util.List;

/**
 * An existential quantification.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class ClassExistQuantification extends ClassExpression {

    private final PropertyExpression property;
    private final Quantifier quantifier;

    /**
     * Creates a ClassExistQuantification object using specified values.
     *
     * @param property   A property expression.
     * @param quantifier A property quantifier.
     */
    public ClassExistQuantification(PropertyExpression property, Quantifier quantifier) {
        this.property = property;
        this.quantifier = quantifier;
    }

    public PropertyExpression getProperty() {
        return property;
    }

    public Quantifier getQuantifier() {
        return quantifier;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        if (property != null) {
            resourceList.addAll(property.listResources());
        }

        if ((quantifier != null) && (quantifier instanceof ClassExpression)) {
            resourceList.addAll(((ClassExpression) quantifier).listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + (this.property != null ? this.property.hashCode() : 0);
        hash = 83 * hash + (this.quantifier != null ? this.quantifier.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClassExistQuantification other = (ClassExistQuantification) obj;
        if (this.property != other.property && (this.property == null || !this.property.equals(other.property))) {
            return false;
        }
        if (this.quantifier != other.quantifier && (this.quantifier == null || !this.quantifier.equals(other.quantifier))) {
            return false;
        }
        return true;
    }
}
