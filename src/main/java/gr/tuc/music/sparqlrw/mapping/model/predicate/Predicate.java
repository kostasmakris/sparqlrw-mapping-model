/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.predicate;

/**
 * The base for a predicate.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public abstract class Predicate {

    private final Operator operator;

    /**
     * Creates a Predicate object using specified values.
     *
     * @param operator The predicate operator.
     */
    public Predicate(Operator operator) {
        this.operator = operator;
    }

    public Operator getOperator() {
        return operator;
    }

    public abstract <T1, T2> T1 accept(PredicateVisitor<T1, T2> visitor, T2 o);

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.operator != null ? this.operator.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Predicate other = (Predicate) obj;
        if (this.operator != other.operator) {
            return false;
        }
        return true;
    }
}
