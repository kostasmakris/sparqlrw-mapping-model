/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import gr.tuc.music.sparqlrw.mapping.model.concept.ClassCardinalityRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassDifference;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassEnumeration;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistQuantification;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExpression;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassIntersection;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassMapping;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUnion;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.condition.AndCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.BasicCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.Condition;
import gr.tuc.music.sparqlrw.mapping.model.condition.ConditionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.condition.NotCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.OrCondition;
import gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceMapping;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.predicate.BinaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.predicate.PredicateVisitor;
import gr.tuc.music.sparqlrw.mapping.model.predicate.UnaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.property.PropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyTransform;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyInverse;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyTransitive;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.value.DataValue;
import gr.tuc.music.sparqlrw.mapping.model.value.ValueVisitor;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import jaxb.sparqlrw.mapping.model.BPredicateType;
import jaxb.sparqlrw.mapping.model.BaseDatatype;
import jaxb.sparqlrw.mapping.model.Class;
import jaxb.sparqlrw.mapping.model.Cmapping;
import jaxb.sparqlrw.mapping.model.Data;
import jaxb.sparqlrw.mapping.model.Dpmapping;
import jaxb.sparqlrw.mapping.model.Dproperty;
import jaxb.sparqlrw.mapping.model.Imapping;
import jaxb.sparqlrw.mapping.model.Individual;
import jaxb.sparqlrw.mapping.model.Model;
import jaxb.sparqlrw.mapping.model.Opmapping;
import jaxb.sparqlrw.mapping.model.Oproperty;
import jaxb.sparqlrw.mapping.model.Resource;
import jaxb.sparqlrw.mapping.model.UPredicateType;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * The SPARQL-RW mapping model serializer (implementing post-order traversal).
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class Serializer implements MappingModelVisitor<Object>, MappingVisitor<Object>, ExpressionVisitor<Object>, ConditionVisitor<Object, Void>, PredicateVisitor<Object, Void>, ValueVisitor<Object> {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(Serializer.class);

    /**
     * Serializes a SPARQL-RW mapping model object to an XML file.
     *
     * @param model A SPARQL-RW mapping model object.
     * @param to    An XML file.
     */
    public void serialize(MappingModel model, File to) {

        if (to == null) {
            log.warn("Input file cannot be null.");
            return;
        }

        try {
            FileUtils.writeStringToFile(to, serialize(model));
        } catch (IOException ex) {
            log.warn("Mapping model failed to stored to the input XML file.");
        }
    }

    /**
     * Serializes a SPARQL-RW mapping model object to an XML string.
     *
     * @param model A SPARQL-RW mapping model object.
     * @return An XML string compatible with the SPARQL-RW mapping model's XML
     *         Schema.
     */
    public String serialize(MappingModel model) {
        String xml = null;

        if (model == null) {
            log.warn("Input mapping model cannot be serialized.");
            return xml;
        }

        // convert SPARQL-RW mapping model to JAXB model
        Model jaxbModel = (Model) model.accept(this);

        // marshall JAXB model to an XML string
        try {
            StringWriter writer = new StringWriter();
            JAXBContext jaxbContext = JAXBContext.newInstance(jaxb.sparqlrw.mapping.model.Model.class);
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource xsdSource = new StreamSource(getClass().getClassLoader().getResourceAsStream("gr/tuc/music/sparqlrw/mapping/model/schema/model.xsd"));
            Schema schema = sf.newSchema(xsdSource);
            Marshaller marshaller = jaxbContext.createMarshaller();

            // set schema for validation
            marshaller.setSchema(schema);

            // output pretty printed
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(jaxbModel, writer);

            xml = writer.toString();
        } catch (JAXBException ex) {
            log.warn("Input mapping model failed to be serialized.", ex);
        } catch (SAXException ex) {
            log.warn("SPARQL-RW mapping model's XSD failed to be loaded.", ex);
        }

        return xml;
    }

    public Object visit(MappingModel o) {
        Model model = new Model();
        model.setFormalism(o.getFormalism().toString());

        if (o.getGlobalOntology() != null) {
            Model.Global global = new Model.Global();
            global.setOntology((jaxb.sparqlrw.mapping.model.Ontology) o.getGlobalOntology().accept(this));
            model.setGlobal(global);
        }

        Model.Locals locals = new Model.Locals();
        model.setLocals(locals);

        for (Ontology ontology : o.getLocalOntologyList()) {
            locals.getOntology().add((jaxb.sparqlrw.mapping.model.Ontology) ontology.accept(this));
        }

        Model.Mappings mappings = new Model.Mappings();
        model.setMappings(mappings);

        for (Mapping mapping : o.getMappingList()) {
            mappings.getCmappingOrOpmappingOrDpmapping().add(mapping.accept(this));
        }

        return model;
    }

    public Object visit(Ontology o) {
        jaxb.sparqlrw.mapping.model.Ontology ontology = new jaxb.sparqlrw.mapping.model.Ontology();

        ontology.setUri(o.getUri());
        ontology.setName(o.getName());
        ontology.setDescription(o.getDescription());
        ontology.setSchemaLocation(o.getSchemaLocation());

        return ontology;
    }

    public Object visit(ClassMapping o) {
        Cmapping.Expr1 expr1 = new Cmapping.Expr1();
        Cmapping.Expr2 expr2 = new Cmapping.Expr2();

        if (o.getExpr1() != null) {
            expr1.setClazz((Class) o.getExpr1().accept(this));
        }
        if (o.getExpr2() != null) {
            expr2.setClazz((Class) o.getExpr2().accept(this));
        }

        Cmapping mapping = new Cmapping();
        mapping.setUri(o.getUri());
        mapping.setExpr1(expr1);
        mapping.setExpr2(expr2);
        mapping.setRelation(o.getRelation().toString());

        return mapping;
    }

    public Object visit(OPropertyMapping o) {
        Opmapping.Expr1 expr1 = new Opmapping.Expr1();
        Opmapping.Expr2 expr2 = new Opmapping.Expr2();

        if (o.getExpr1() != null) {
            expr1.setOproperty((Oproperty) o.getExpr1().accept(this));
        }
        if (o.getExpr2() != null) {
            expr2.setOproperty((Oproperty) o.getExpr2().accept(this));
        }

        Opmapping mapping = new Opmapping();
        mapping.setUri(o.getUri());
        mapping.setExpr1(expr1);
        mapping.setExpr2(expr2);
        mapping.setRelation(o.getRelation().toString());

        return mapping;
    }

    public Object visit(DPropertyMapping o) {
        Dpmapping.Expr1 expr1 = new Dpmapping.Expr1();
        Dpmapping.Expr2 expr2 = new Dpmapping.Expr2();

        if (o.getExpr1() != null) {
            expr1.setDproperty((Dproperty) o.getExpr1().accept(this));
        }
        if (o.getExpr2() != null) {
            expr2.setDproperty((Dproperty) o.getExpr2().accept(this));
        }

        Dpmapping mapping = new Dpmapping();
        mapping.setUri(o.getUri());
        mapping.setExpr1(expr1);
        mapping.setExpr2(expr2);
        mapping.setRelation(o.getRelation().toString());

        return mapping;
    }

    public Object visit(InstanceMapping o) {
        Imapping.Expr1 expr1 = new Imapping.Expr1();
        Imapping.Expr2 expr2 = new Imapping.Expr2();

        if (o.getExpr1() != null) {
            expr1.setIndividual((Individual) o.getExpr1().accept((ExpressionVisitor) this));
        }
        if (o.getExpr2() != null) {
            expr2.setIndividual((Individual) o.getExpr2().accept((ExpressionVisitor) this));
        }

        Imapping mapping = new Imapping();
        mapping.setUri(o.getUri());
        mapping.setExpr1(expr1);
        mapping.setExpr2(expr2);
        mapping.setRelation(o.getRelation().toString());

        return mapping;
    }

    public Object visit(ClassUri o) {
        Resource resource = new Resource();
        resource.setUri(o.getUri());

        Class clazz = new Class();
        clazz.setResource(resource);

        return clazz;
    }

    public Object visit(ClassUnion o) {
        Class.Union union = new Class.Union();

        for (ClassExpression expr : o.getOperandList()) {
            union.getClazz().add((Class) expr.accept(this));
        }

        Class clazz = new Class();
        clazz.setUnion(union);
        return clazz;
    }

    public Object visit(ClassIntersection o) {
        Class.Intersection intersection = new Class.Intersection();

        for (ClassExpression expr : o.getOperandList()) {
            intersection.getClazz().add((Class) expr.accept(this));
        }

        Class clazz = new Class();
        clazz.setIntersection(intersection);
        return clazz;
    }

    public Object visit(ClassDifference o) {
        Class.Difference difference = new Class.Difference();

        for (ClassExpression expr : o.getOperandList()) {
            difference.getClazz().add((Class) expr.accept(this));
        }

        Class clazz = new Class();
        clazz.setDifference(difference);
        return clazz;
    }

    public Object visit(ClassEnumeration o) {
        Class.Enumeration enumeration = new Class.Enumeration();

        for (InstanceUri instance : o.getOperandList()) {
            enumeration.getIndividual().add((Individual) instance.accept((ExpressionVisitor) this));
        }

        Class clazz = new Class();
        clazz.setEnumeration(enumeration);
        return clazz;
    }

    public Object visit(ClassCardinalityRestriction o) {
        Class.Cardinality cardinality = new Class.Cardinality();

        if (o.getProperty() == null) {
            return null;
        }

        if (o.getProperty() instanceof OPropertyExpression) {
            Class.Cardinality.OnOProperty onOProperty = new Class.Cardinality.OnOProperty();

            onOProperty.setOproperty((Oproperty) o.getProperty().accept(this));

            if (o.getQuantifier() != null) {
                Class.Cardinality.OnOProperty.Quantifier quantifier = new Class.Cardinality.OnOProperty.Quantifier();
                quantifier.setClazz((Class) o.getQuantifier().accept(this));
                onOProperty.setQuantifier(quantifier);
            }

            if (o.getCardinality() != null) {
                onOProperty.setPredicate((UPredicateType) o.getCardinality().accept(this, null));
            }

            cardinality.setOnOProperty(onOProperty);
        } else {
            Class.Cardinality.OnDProperty onDProperty = new Class.Cardinality.OnDProperty();

            onDProperty.setDproperty((Dproperty) o.getProperty().accept(this));

            if (o.getQuantifier() != null) {
                Class.Cardinality.OnDProperty.Quantifier quantifier = new Class.Cardinality.OnDProperty.Quantifier();
                quantifier.setDatatype((jaxb.sparqlrw.mapping.model.Datatype) o.getQuantifier().accept(this));
                onDProperty.setQuantifier(quantifier);
            }

            if (o.getCardinality() != null) {
                onDProperty.setPredicate((UPredicateType) o.getCardinality().accept(this, null));
            }

            cardinality.setOnDProperty(onDProperty);
        }

        Class clazz = new Class();
        clazz.setCardinality(cardinality);
        return clazz;
    }

    public Object visit(ClassExistPredRestriction o) {
        Class.Existpred existpred = new Class.Existpred();

        if ((o.getOperandList() == null) || o.getOperandList().isEmpty()) {
            return null;
        }

        if ((o.getOperandList().get(0) instanceof OPropertyExpression) && (o.getOperandList().size() == 1)) {
            Class.Existpred.OnOProperty onOProperty = new Class.Existpred.OnOProperty();

            onOProperty.setOproperty((Oproperty) o.getOperandList().get(0).accept(this));

            if (o.getPredicate() != null) {
                onOProperty.setPredicate((UPredicateType) o.getPredicate().accept(this, null));
            }

            existpred.setOnOProperty(onOProperty);
        } else if ((o.getOperandList().get(0) instanceof OPropertyExpression) && (o.getOperandList().size() > 1)) {
            Class.Existpred.OnOProperties onOProperties = new Class.Existpred.OnOProperties();

            for (PropertyExpression property : o.getOperandList()) {
                onOProperties.getOproperty().add((Oproperty) property.accept(this));
            }

            if (o.getPredicate() != null) {
                onOProperties.setPredicate((BPredicateType) o.getPredicate().accept(this, null));
            }

            existpred.setOnOProperties(onOProperties);
        } else if ((o.getOperandList().get(0) instanceof DPropertyExpression) && (o.getOperandList().size() == 1)) {
            Class.Existpred.OnDProperty onDProperty = new Class.Existpred.OnDProperty();

            onDProperty.setDproperty((Dproperty) o.getOperandList().get(0).accept(this));

            if (o.getPredicate() != null) {
                onDProperty.setPredicate((UPredicateType) o.getPredicate().accept(this, null));
            }

            existpred.setOnDProperty(onDProperty);
        } else if ((o.getOperandList().get(0) instanceof DPropertyExpression) && (o.getOperandList().size() > 1)) {
            Class.Existpred.OnDProperties onDProperties = new Class.Existpred.OnDProperties();

            for (PropertyExpression property : o.getOperandList()) {
                onDProperties.getDproperty().add((Dproperty) property.accept(this));
            }

            if (o.getPredicate() != null) {
                onDProperties.setPredicate((BPredicateType) o.getPredicate().accept(this, null));
            }

            existpred.setOnDProperties(onDProperties);
        }

        Class clazz = new Class();
        clazz.setExistpred(existpred);
        return clazz;
    }

    public Object visit(ClassExistQuantification o) {
        Class.Existquant existquant = new Class.Existquant();

        if (o.getProperty() == null) {
            return null;
        }

        if (o.getProperty() instanceof OPropertyExpression) {
            Class.Existquant.OnOProperty onOProperty = new Class.Existquant.OnOProperty();

            onOProperty.setOproperty((Oproperty) o.getProperty().accept(this));

            if (o.getQuantifier() != null) {
                Class.Existquant.OnOProperty.Quantifier quantifier = new Class.Existquant.OnOProperty.Quantifier();
                quantifier.setClazz((Class) o.getQuantifier().accept(this));
                onOProperty.setQuantifier(quantifier);
            }

            existquant.setOnOProperty(onOProperty);
        } else if (o.getProperty() instanceof DPropertyExpression) {
            Class.Existquant.OnDProperty onDProperty = new Class.Existquant.OnDProperty();

            onDProperty.setDproperty((Dproperty) o.getProperty().accept(this));

            if (o.getQuantifier() != null) {
                Class.Existquant.OnDProperty.Quantifier quantifier = new Class.Existquant.OnDProperty.Quantifier();
                quantifier.setDatatype((jaxb.sparqlrw.mapping.model.Datatype) o.getQuantifier().accept(this));
                onDProperty.setQuantifier(quantifier);
            }

            existquant.setOnDProperty(onDProperty);
        }

        Class clazz = new Class();
        clazz.setExistquant(existquant);
        return clazz;
    }

    public Object visit(OPropertyUri o) {
        Resource resource = new Resource();
        resource.setUri(o.getUri());

        Oproperty property = new Oproperty();
        property.setResource(resource);

        return property;
    }

    public Object visit(OPropertyUnion o) {
        Oproperty.Union union = new Oproperty.Union();

        for (OPropertyExpression expr : o.getOperandList()) {
            union.getOproperty().add((Oproperty) expr.accept(this));
        }

        Oproperty property = new Oproperty();
        property.setUnion(union);
        return property;
    }

    public Object visit(OPropertyIntersection o) {
        Oproperty.Intersection intersection = new Oproperty.Intersection();

        for (OPropertyExpression expr : o.getOperandList()) {
            intersection.getOproperty().add((Oproperty) expr.accept(this));
        }

        Oproperty property = new Oproperty();
        property.setIntersection(intersection);
        return property;
    }

    public Object visit(OPropertyDifference o) {
        Oproperty.Difference difference = new Oproperty.Difference();

        for (OPropertyExpression expr : o.getOperandList()) {
            difference.getOproperty().add((Oproperty) expr.accept(this));
        }

        Oproperty property = new Oproperty();
        property.setDifference(difference);
        return property;
    }

    public Object visit(OPropertyComposition o) {
        Oproperty.Composition composition = new Oproperty.Composition();

        for (OPropertyExpression expr : o.getOperandList()) {
            composition.getOproperty().add((Oproperty) expr.accept(this));
        }

        Oproperty property = new Oproperty();
        property.setComposition(composition);
        return property;
    }

    public Object visit(OPropertyInverse o) {
        Oproperty.Inverse inverse = new Oproperty.Inverse();

        if (o.getOperand() != null) {
            inverse.setOproperty((Oproperty) o.getOperand().accept(this));
        }

        Oproperty property = new Oproperty();
        property.setInverse(inverse);
        return property;
    }

    public Object visit(OPropertyTransitive o) {
        Oproperty.Transitive transitive = new Oproperty.Transitive();

        if (o.getOperand() != null) {
            transitive.setOproperty((Oproperty) o.getOperand().accept(this));
        }

        Oproperty property = new Oproperty();
        property.setTransitive(transitive);
        return property;
    }

    public Object visit(OPropertyRestriction o) {
        Oproperty.Restrict restrict = new Oproperty.Restrict();

        if (o.getProperty() != null) {
            restrict.setOproperty((Oproperty) o.getProperty().accept(this));
        }

        if (o.getDomain() != null) {
            Oproperty.Restrict.Domain domain = new Oproperty.Restrict.Domain();
            domain.setClazz((Class) o.getDomain().accept(this));
            restrict.setDomain(domain);
        }

        if (o.getRange() != null) {
            Oproperty.Restrict.Range range = new Oproperty.Restrict.Range();
            range.setClazz((Class) o.getRange().accept(this));
            restrict.setRange(range);
        }

        Oproperty property = new Oproperty();
        property.setRestrict(restrict);
        return property;
    }

    public Object visit(OPropertyExistPredRestriction o) {
        Oproperty.Existpred existpred = new Oproperty.Existpred();

        if ((o.getOperand1List() == null) || o.getOperand1List().isEmpty()) {
            return null;
        }

        if (o.getOperand1List().get(0) instanceof OPropertyExpression) {
            Oproperty.Existpred.OnOProperties onOProperties = new Oproperty.Existpred.OnOProperties();

            for (PropertyExpression property : o.getOperand1List()) {
                onOProperties.getOproperty().add((Oproperty) property.accept(this));
            }

            for (PropertyExpression property : o.getOperand2List()) {
                onOProperties.getOproperty().add((Oproperty) property.accept(this));
            }

            if (o.getPredicate() != null) {
                onOProperties.setPredicate((BPredicateType) o.getPredicate().accept(this, null));
            }

            existpred.setOnOProperties(onOProperties);
        } else {
            Oproperty.Existpred.OnDProperties onDProperties = new Oproperty.Existpred.OnDProperties();

            for (PropertyExpression property : o.getOperand1List()) {
                onDProperties.getDproperty().add((Dproperty) property.accept(this));
            }

            for (PropertyExpression property : o.getOperand2List()) {
                onDProperties.getDproperty().add((Dproperty) property.accept(this));
            }

            if (o.getPredicate() != null) {
                onDProperties.setPredicate((BPredicateType) o.getPredicate().accept(this, null));
            }

            existpred.setOnDProperties(onDProperties);
        }

        Oproperty property = new Oproperty();
        property.setExistpred(existpred);
        return property;
    }

    public Object visit(DPropertyUri o) {
        Resource resource = new Resource();
        resource.setUri(o.getUri());

        Dproperty property = new Dproperty();
        property.setResource(resource);

        return property;
    }

    public Object visit(DPropertyUnion o) {
        Dproperty.Union union = new Dproperty.Union();

        for (DPropertyExpression expr : o.getOperandList()) {
            union.getDproperty().add((Dproperty) expr.accept(this));
        }

        Dproperty property = new Dproperty();
        property.setUnion(union);
        return property;
    }

    public Object visit(DPropertyIntersection o) {
        Dproperty.Intersection intersection = new Dproperty.Intersection();

        for (DPropertyExpression expr : o.getOperandList()) {
            intersection.getDproperty().add((Dproperty) expr.accept(this));
        }

        Dproperty property = new Dproperty();
        property.setIntersection(intersection);
        return property;
    }

    public Object visit(DPropertyDifference o) {
        Dproperty.Difference difference = new Dproperty.Difference();

        for (DPropertyExpression expr : o.getOperandList()) {
            difference.getDproperty().add((Dproperty) expr.accept(this));
        }

        Dproperty property = new Dproperty();
        property.setDifference(difference);
        return property;
    }

    public Object visit(DPropertyComposition o) {
        Dproperty.Composition composition = new Dproperty.Composition();

        for (OPropertyExpression property : o.getInitOperandList()) {
            composition.getOproperty().add((Oproperty) property.accept(this));
        }

        if (o.getLastOperand() != null) {
            composition.setDproperty((Dproperty) o.getLastOperand().accept(this));
        }

        Dproperty property = new Dproperty();
        property.setComposition(composition);
        return property;
    }

    public Object visit(DPropertyTransform o) {
        Dproperty.Transform transform = new Dproperty.Transform();

        if (o.getProperty() != null) {
            transform.setDproperty((Dproperty) o.getProperty().accept(this));
        }

        transform.setTo(BaseDatatype.valueOf(o.getDatatype().toString()));

        Dproperty property = new Dproperty();
        property.setTransform(transform);
        return property;
    }

    public Object visit(DPropertyRestriction o) {
        Dproperty.Restrict restrict = new Dproperty.Restrict();

        if (o.getProperty() != null) {
            restrict.setDproperty((Dproperty) o.getProperty().accept(this));
        }

        if (o.getDomain() != null) {
            Dproperty.Restrict.Domain domain = new Dproperty.Restrict.Domain();
            domain.setClazz((Class) o.getDomain().accept(this));
            restrict.setDomain(domain);
        }

        if (o.getRange() != null) {
            Dproperty.Restrict.Range range = new Dproperty.Restrict.Range();
            range.setDatatype((jaxb.sparqlrw.mapping.model.Datatype) o.getRange().accept(this));
            restrict.setRange(range);
        }

        Dproperty property = new Dproperty();
        property.setRestrict(restrict);
        return property;
    }

    public Object visit(InstanceUri o) {
        Resource resource = new Resource();
        resource.setUri(o.getUri());

        Individual individual = new Individual();
        individual.setResource(resource);

        return individual;
    }

    public Object visit(Datatype o) {
        jaxb.sparqlrw.mapping.model.Datatype datatype = new jaxb.sparqlrw.mapping.model.Datatype();

        if (o.getBase() != null) {
            datatype.setBase(BaseDatatype.valueOf(o.getBase().toString()));
        }

        if (o.getRestriction() != null) {
            datatype.setCondition((jaxb.sparqlrw.mapping.model.Condition) o.getRestriction().accept(this, null));
        }

        return datatype;
    }

    public Object visit(BasicCondition c, Void o) {
        if (c.getRestriction() != null) {
            jaxb.sparqlrw.mapping.model.Condition condition = new jaxb.sparqlrw.mapping.model.Condition();
            condition.setBasic((UPredicateType) c.getRestriction().accept(this, o));
            return condition;
        }

        return null;
    }

    public Object visit(AndCondition c, Void o) {
        jaxb.sparqlrw.mapping.model.Condition.And and = new jaxb.sparqlrw.mapping.model.Condition.And();

        for (Condition condition : c.getOperandList()) {
            and.getCondition().add((jaxb.sparqlrw.mapping.model.Condition) condition.accept(this, o));
        }

        jaxb.sparqlrw.mapping.model.Condition condition = new jaxb.sparqlrw.mapping.model.Condition();
        condition.setAnd(and);
        return condition;
    }

    public Object visit(OrCondition c, Void o) {
        jaxb.sparqlrw.mapping.model.Condition.Or or = new jaxb.sparqlrw.mapping.model.Condition.Or();

        for (Condition condition : c.getOperandList()) {
            or.getCondition().add((jaxb.sparqlrw.mapping.model.Condition) condition.accept(this, o));
        }

        jaxb.sparqlrw.mapping.model.Condition condition = new jaxb.sparqlrw.mapping.model.Condition();
        condition.setOr(or);
        return condition;
    }

    public Object visit(NotCondition c, Void o) {
        jaxb.sparqlrw.mapping.model.Condition.Not not = new jaxb.sparqlrw.mapping.model.Condition.Not();

        if (c.getOperand() != null) {
            not.setCondition((jaxb.sparqlrw.mapping.model.Condition) c.getOperand().accept(this, o));
        }

        jaxb.sparqlrw.mapping.model.Condition condition = new jaxb.sparqlrw.mapping.model.Condition();
        condition.setNot(not);
        return condition;
    }

    public Object visit(BinaryPredicate p, Void o) {
        BPredicateType bpred = new BPredicateType();
        bpred.setOperator(p.getOperator().toString());
        return bpred;
    }

    public Object visit(UnaryPredicate p, Void o) {
        UPredicateType upred = new UPredicateType();

        if (p.getValue() != null) {
            Object temp = p.getValue().accept((ValueVisitor) this);

            jaxb.sparqlrw.mapping.model.Value value = new jaxb.sparqlrw.mapping.model.Value();

            if (temp instanceof Individual) {
                value.setIndividual((Individual) temp);
            } else {
                value.setData((Data) temp);
            }

            upred.setValue(value);
        }

        upred.setOperator(p.getOperator().toString());
        return upred;
    }

    public Object visit(DataValue v) {
        Data data = new Data();
        data.setValue(v.getValue());
        data.setType(BaseDatatype.valueOf(v.getDatatype().toString()));

        return data;
    }
}
