/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.property.object;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import gr.tuc.music.sparqlrw.mapping.model.predicate.Predicate;
import gr.tuc.music.sparqlrw.mapping.model.property.PropertyExpression;
import java.util.ArrayList;
import java.util.List;

/**
 * An existential predicate restriction on property values.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class OPropertyExistPredRestriction extends OPropertyExpression {

    private final List<PropertyExpression> operand1List;
    private final List<PropertyExpression> operand2List;
    private final Predicate predicate;

    /**
     * Creates an OPropertyExistPredRestriction object using specified values.
     *
     * @param operand1List A list of property expressions.
     * @param operand2List A list of property expressions.
     * @param predicate    A predicate .
     */
    public OPropertyExistPredRestriction(List<PropertyExpression> operand1List, List<PropertyExpression> operand2List, Predicate predicate) {
        this.operand1List = new ArrayList<PropertyExpression>(operand1List);
        this.operand2List = new ArrayList<PropertyExpression>(operand2List);
        this.predicate = predicate;
    }

    public List<PropertyExpression> getOperand1List() {
        return operand1List;
    }

    public List<PropertyExpression> getOperand2List() {
        return operand2List;
    }

    public Predicate getPredicate() {
        return predicate;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        for (PropertyExpression expr : operand1List) {
            resourceList.addAll(expr.listResources());
        }

        for (PropertyExpression expr : operand2List) {
            resourceList.addAll(expr.listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.operand1List != null ? this.operand1List.hashCode() : 0);
        hash = 37 * hash + (this.operand2List != null ? this.operand2List.hashCode() : 0);
        hash = 37 * hash + (this.predicate != null ? this.predicate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OPropertyExistPredRestriction other = (OPropertyExistPredRestriction) obj;
        if (this.operand1List != other.operand1List && (this.operand1List == null || !this.operand1List.equals(other.operand1List))) {
            return false;
        }
        if (this.operand2List != other.operand2List && (this.operand2List == null || !this.operand2List.equals(other.operand2List))) {
            return false;
        }
        if (this.predicate != other.predicate && (this.predicate == null || !this.predicate.equals(other.predicate))) {
            return false;
        }
        return true;
    }
}
