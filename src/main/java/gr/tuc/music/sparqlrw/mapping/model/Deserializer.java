/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import gr.tuc.music.sparqlrw.mapping.model.concept.ClassCardinalityRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassDifference;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassEnumeration;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistQuantification;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExpression;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassIntersection;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassMapping;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUnion;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.concept.Quantifier;
import gr.tuc.music.sparqlrw.mapping.model.condition.AndCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.BasicCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.NotCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.OrCondition;
import gr.tuc.music.sparqlrw.mapping.model.datarange.XmlDatatype;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceMapping;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.predicate.BinaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.predicate.Operator;
import gr.tuc.music.sparqlrw.mapping.model.predicate.Predicate;
import gr.tuc.music.sparqlrw.mapping.model.predicate.UnaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.property.PropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyTransform;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyInverse;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyTransitive;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.value.DataValue;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import jaxb.sparqlrw.mapping.model.BPredicateType;
import jaxb.sparqlrw.mapping.model.Class;
import jaxb.sparqlrw.mapping.model.Cmapping;
import jaxb.sparqlrw.mapping.model.Condition;
import jaxb.sparqlrw.mapping.model.Data;
import jaxb.sparqlrw.mapping.model.Datatype;
import jaxb.sparqlrw.mapping.model.Dpmapping;
import jaxb.sparqlrw.mapping.model.Dproperty;
import jaxb.sparqlrw.mapping.model.Imapping;
import jaxb.sparqlrw.mapping.model.Individual;
import jaxb.sparqlrw.mapping.model.Model;
import jaxb.sparqlrw.mapping.model.Ontology;
import jaxb.sparqlrw.mapping.model.Opmapping;
import jaxb.sparqlrw.mapping.model.Oproperty;
import jaxb.sparqlrw.mapping.model.Resource;
import jaxb.sparqlrw.mapping.model.UPredicateType;
import jaxb.sparqlrw.mapping.model.Value;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * The SPARQL-RW mapping model deserializer (implementing post-order traversal).
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class Deserializer implements jaxb.sparqlrw.mapping.model.visitor.Visitor {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(Deserializer.class);

    private Object result;

    /**
     * Deserializes an input XML file to a SPARQL-RW mapping model object.
     *
     * @param file An XML file compatible with the SPARQL-RW mapping model's XML
     *             Schema.
     * @return A SPARQL-RW mapping model object.
     */
    public MappingModel deserialize(File file) {
        String xml;

        try {
            xml = FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException ex) {
            log.warn("Mapping file not found.", ex);
            return null;
        }

        return deserialize(xml);
    }

    /**
     * Deserializes an input XML string to a SPARQL-RW mapping model object.
     *
     * @param xml An XML string compatible with the SPARQL-RW mapping model's
     *            XML Schema.
     * @return A SPARQL-RW mapping model object.
     */
    public MappingModel deserialize(String xml) {
        MappingModel model = null;

        if (xml == null) {
            log.warn("Input XML string cannot be deserialized.");
            return model;
        }

        // unmarshall input XML string to JAXB model
        jaxb.sparqlrw.mapping.model.Model jaxbModel;
        try {
            StringReader reader = new StringReader(xml);
            JAXBContext jaxbContext = JAXBContext.newInstance(jaxb.sparqlrw.mapping.model.Model.class);
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            StreamSource xsdSource = new StreamSource(getClass().getClassLoader().getResourceAsStream("gr/tuc/music/sparqlrw/mapping/model/schema/model.xsd"));
            Schema schema = sf.newSchema(xsdSource);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            // set schema for validation
            unmarshaller.setSchema(schema);
            jaxbModel = (jaxb.sparqlrw.mapping.model.Model) unmarshaller.unmarshal(reader);
        } catch (JAXBException ex) {
            log.warn("Input XML string failed to be deserialized.", ex);
            return model;
        } catch (SAXException ex) {
            log.warn("SPARQL-RW mapping model's XSD failed to be loaded.", ex);
            return model;
        }

        // convert JAXB model to SPARQL-RW mapping model
        jaxbModel.accept(this);
        model = (MappingModel) result;

        return model;
    }

    public void visit(BPredicateType aBean) {
        result = new BinaryPredicate(Operator.valueOf(aBean.getOperator()));
    }

    public void visit(Class aBean) {
        if (aBean.getResource() != null) {
            aBean.getResource().accept(this);
            result = new ClassUri((String) result);
        }
        if (aBean.getUnion() != null) {
            aBean.getUnion().accept(this);
        }
        if (aBean.getIntersection() != null) {
            aBean.getIntersection().accept(this);
        }
        if (aBean.getDifference() != null) {
            aBean.getDifference().accept(this);
        }
        if (aBean.getEnumeration() != null) {
            aBean.getEnumeration().accept(this);
        }
        if (aBean.getExistquant() != null) {
            aBean.getExistquant().accept(this);
        }
        if (aBean.getExistpred() != null) {
            aBean.getExistpred().accept(this);
        }
        if (aBean.getCardinality() != null) {
            aBean.getCardinality().accept(this);
        }
    }

    public void visit(Class.Cardinality aBean) {
        if (aBean.getOnOProperty() != null) {
            aBean.getOnOProperty().accept(this);
        }
        if (aBean.getOnDProperty() != null) {
            aBean.getOnDProperty().accept(this);
        }
    }

    public void visit(Class.Cardinality.OnDProperty aBean) {
        PropertyExpression property = null;
        UnaryPredicate predicate = null;
        Quantifier quantifier = null;

        if (aBean.getDproperty() != null) {
            aBean.getDproperty().accept(this);
            property = (PropertyExpression) result;
        }
        if (aBean.getPredicate() != null) {
            aBean.getPredicate().accept(this);
            predicate = (UnaryPredicate) result;
        }
        if (aBean.getQuantifier() != null) {
            aBean.getQuantifier().accept(this);
            quantifier = (Quantifier) result;
        }

        result = new ClassCardinalityRestriction(property, quantifier, predicate);
    }

    public void visit(Class.Cardinality.OnDProperty.Quantifier aBean) {
        if (aBean.getDatatype() != null) {
            aBean.getDatatype().accept(this);
        }
    }

    public void visit(Class.Cardinality.OnOProperty aBean) {
        PropertyExpression property = null;
        UnaryPredicate predicate = null;
        Quantifier quantifier = null;

        if (aBean.getOproperty() != null) {
            aBean.getOproperty().accept(this);
            property = (PropertyExpression) result;
        }
        if (aBean.getPredicate() != null) {
            aBean.getPredicate().accept(this);
            predicate = (UnaryPredicate) result;
        }
        if (aBean.getQuantifier() != null) {
            aBean.getQuantifier().accept(this);
            quantifier = (Quantifier) result;
        }

        result = new ClassCardinalityRestriction(property, quantifier, predicate);
    }

    public void visit(Class.Cardinality.OnOProperty.Quantifier aBean) {
        if (aBean.getClazz() != null) {
            aBean.getClazz().accept(this);
        }
    }

    public void visit(Class.Difference aBean) {
        List<ClassExpression> classList = new ArrayList<ClassExpression>();

        for (Class bean : aBean.getClazz()) {
            bean.accept(this);
            classList.add((ClassExpression) result);
        }

        result = new ClassDifference(classList);
    }

    public void visit(Class.Enumeration aBean) {
        List<InstanceUri> instanceList = new ArrayList<InstanceUri>();

        for (Individual bean : aBean.getIndividual()) {
            bean.accept(this);
            instanceList.add((InstanceUri) result);
        }

        result = new ClassEnumeration(instanceList);
    }

    public void visit(Class.Existpred aBean) {
        if (aBean.getOnOProperty() != null) {
            aBean.getOnOProperty().accept(this);
        }
        if (aBean.getOnDProperty() != null) {
            aBean.getOnDProperty().accept(this);
        }
        if (aBean.getOnOProperties() != null) {
            aBean.getOnOProperties().accept(this);
        }
        if (aBean.getOnDProperties() != null) {
            aBean.getOnDProperties().accept(this);
        }
    }

    public void visit(Class.Existpred.OnDProperties aBean) {
        List<PropertyExpression> propertyList = new ArrayList<PropertyExpression>();
        Predicate predicate = null;

        for (Dproperty bean : aBean.getDproperty()) {
            bean.accept(this);
            propertyList.add((DPropertyExpression) result);
        }
        if (aBean.getPredicate() != null) {
            aBean.getPredicate().accept(this);
            predicate = (Predicate) result;
        }

        result = new ClassExistPredRestriction(propertyList, predicate);
    }

    public void visit(Class.Existpred.OnDProperty aBean) {
        List<PropertyExpression> propertyList = new ArrayList<PropertyExpression>();
        Predicate predicate = null;

        if (aBean.getDproperty() != null) {
            aBean.getDproperty().accept(this);
            propertyList.add((DPropertyExpression) result);
        }
        if (aBean.getPredicate() != null) {
            aBean.getPredicate().accept(this);
            predicate = (Predicate) result;
        }

        result = new ClassExistPredRestriction(propertyList, predicate);
    }

    public void visit(Class.Existpred.OnOProperties aBean) {
        List<PropertyExpression> propertyList = new ArrayList<PropertyExpression>();
        Predicate predicate = null;

        for (Oproperty bean : aBean.getOproperty()) {
            bean.accept(this);
            propertyList.add((OPropertyExpression) result);
        }
        if (aBean.getPredicate() != null) {
            aBean.getPredicate().accept(this);
            predicate = (Predicate) result;
        }

        result = new ClassExistPredRestriction(propertyList, predicate);
    }

    public void visit(Class.Existpred.OnOProperty aBean) {
        List<PropertyExpression> propertyList = new ArrayList<PropertyExpression>();
        Predicate predicate = null;

        if (aBean.getOproperty() != null) {
            aBean.getOproperty().accept(this);
            propertyList.add((OPropertyExpression) result);
        }
        if (aBean.getPredicate() != null) {
            aBean.getPredicate().accept(this);
            predicate = (Predicate) result;
        }

        result = new ClassExistPredRestriction(propertyList, predicate);
    }

    public void visit(Class.Existquant aBean) {
        if (aBean.getOnOProperty() != null) {
            aBean.getOnOProperty().accept(this);
        }
        if (aBean.getOnDProperty() != null) {
            aBean.getOnDProperty().accept(this);
        }
    }

    public void visit(Class.Existquant.OnDProperty aBean) {
        PropertyExpression property = null;
        Quantifier quantifier = null;

        if (aBean.getDproperty() != null) {
            aBean.getDproperty().accept(this);
            property = (PropertyExpression) result;
        }
        if (aBean.getQuantifier() != null) {
            aBean.getQuantifier().accept(this);
            quantifier = (Quantifier) result;
        }

        result = new ClassExistQuantification(property, quantifier);
    }

    public void visit(Class.Existquant.OnDProperty.Quantifier aBean) {
        if (aBean.getDatatype() != null) {
            aBean.getDatatype().accept(this);
        }
    }

    public void visit(Class.Existquant.OnOProperty aBean) {
        PropertyExpression property = null;
        Quantifier quantifier = null;

        if (aBean.getOproperty() != null) {
            aBean.getOproperty().accept(this);
            property = (PropertyExpression) result;
        }
        if (aBean.getQuantifier() != null) {
            aBean.getQuantifier().accept(this);
            quantifier = (Quantifier) result;
        }

        result = new ClassExistQuantification(property, quantifier);
    }

    public void visit(Class.Existquant.OnOProperty.Quantifier aBean) {
        if (aBean.getClazz() != null) {
            aBean.getClazz().accept(this);
        }
    }

    public void visit(Class.Intersection aBean) {
        List<ClassExpression> classList = new ArrayList<ClassExpression>();

        for (Class bean : aBean.getClazz()) {
            bean.accept(this);
            classList.add((ClassExpression) result);
        }

        result = new ClassIntersection(classList);
    }

    public void visit(Class.Union aBean) {
        List<ClassExpression> classList = new ArrayList<ClassExpression>();

        for (Class bean : aBean.getClazz()) {
            bean.accept(this);
            classList.add((ClassExpression) result);
        }

        result = new ClassUnion(classList);
    }

    public void visit(Cmapping aBean) {
        ClassExpression expr1 = null;
        ClassExpression expr2 = null;

        if (aBean.getExpr1() != null) {
            aBean.getExpr1().accept(this);
            expr1 = (ClassExpression) result;
        }
        if (aBean.getExpr2() != null) {
            aBean.getExpr2().accept(this);
            expr2 = (ClassExpression) result;
        }

        result = new ClassMapping(aBean.getUri(), expr1, expr2, Relation.valueOf(aBean.getRelation()));
    }

    public void visit(Cmapping.Expr1 aBean) {
        if (aBean.getClazz() != null) {
            aBean.getClazz().accept(this);
        }
    }

    public void visit(Cmapping.Expr2 aBean) {
        if (aBean.getClazz() != null) {
            aBean.getClazz().accept(this);
        }
    }

    public void visit(Condition aBean) {
        if (aBean.getBasic() != null) {
            aBean.getBasic().accept(this);
            result = new BasicCondition((UnaryPredicate) result);
        }
        if (aBean.getAnd() != null) {
            aBean.getAnd().accept(this);
        }
        if (aBean.getOr() != null) {
            aBean.getOr().accept(this);
        }
        if (aBean.getNot() != null) {
            aBean.getNot().accept(this);
        }
    }

    public void visit(Condition.And aBean) {
        List<gr.tuc.music.sparqlrw.mapping.model.condition.Condition> conditionList = new ArrayList<gr.tuc.music.sparqlrw.mapping.model.condition.Condition>();

        for (Condition bean : aBean.getCondition()) {
            bean.accept(this);
            conditionList.add((gr.tuc.music.sparqlrw.mapping.model.condition.Condition) result);
        }

        result = new AndCondition(conditionList);
    }

    public void visit(Condition.Not aBean) {
        gr.tuc.music.sparqlrw.mapping.model.condition.Condition operand = null;

        if (aBean.getCondition() != null) {
            aBean.getCondition().accept(this);
            operand = (gr.tuc.music.sparqlrw.mapping.model.condition.Condition) result;
        }

        result = new NotCondition(operand);
    }

    public void visit(Condition.Or aBean) {
        List<gr.tuc.music.sparqlrw.mapping.model.condition.Condition> conditionList = new ArrayList<gr.tuc.music.sparqlrw.mapping.model.condition.Condition>();

        for (Condition bean : aBean.getCondition()) {
            bean.accept(this);
            conditionList.add((gr.tuc.music.sparqlrw.mapping.model.condition.Condition) result);
        }

        result = new OrCondition(conditionList);
    }

    public void visit(Data aBean) {
        result = new DataValue(aBean.getValue(), XmlDatatype.valueOf(aBean.getType().value()));
    }

    public void visit(Datatype aBean) {
        XmlDatatype datatype = null;
        gr.tuc.music.sparqlrw.mapping.model.condition.Condition condition = null;

        if (aBean.getBase() != null) {
            datatype = XmlDatatype.valueOf(aBean.getBase().value());
        }

        if (aBean.getCondition() != null) {
            aBean.getCondition().accept(this);
            condition = (gr.tuc.music.sparqlrw.mapping.model.condition.Condition) result;
        }

        result = new gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype(datatype, condition);
    }

    public void visit(Dpmapping aBean) {
        DPropertyExpression expr1 = null;
        DPropertyExpression expr2 = null;

        if (aBean.getExpr1() != null) {
            aBean.getExpr1().accept(this);
            expr1 = (DPropertyExpression) result;
        }
        if (aBean.getExpr2() != null) {
            aBean.getExpr2().accept(this);
            expr2 = (DPropertyExpression) result;
        }

        result = new DPropertyMapping(aBean.getUri(), expr1, expr2, Relation.valueOf(aBean.getRelation()));
    }

    public void visit(Dpmapping.Expr1 aBean) {
        if (aBean.getDproperty() != null) {
            aBean.getDproperty().accept(this);
        }
    }

    public void visit(Dpmapping.Expr2 aBean) {
        if (aBean.getDproperty() != null) {
            aBean.getDproperty().accept(this);
        }
    }

    public void visit(Dproperty aBean) {
        if (aBean.getResource() != null) {
            aBean.getResource().accept(this);
            result = new DPropertyUri((String) result);
        }
        if (aBean.getUnion() != null) {
            aBean.getUnion().accept(this);
        }
        if (aBean.getIntersection() != null) {
            aBean.getIntersection().accept(this);
        }
        if (aBean.getDifference() != null) {
            aBean.getDifference().accept(this);
        }
        if (aBean.getComposition() != null) {
            aBean.getComposition().accept(this);
        }
        if (aBean.getTransform() != null) {
            aBean.getTransform().accept(this);
        }
        if (aBean.getRestrict() != null) {
            aBean.getRestrict().accept(this);
        }
    }

    public void visit(Dproperty.Composition aBean) {
        List<OPropertyExpression> initOperandList = new ArrayList<OPropertyExpression>();
        DPropertyExpression lastOperand = null;

        for (Oproperty bean : aBean.getOproperty()) {
            bean.accept(this);
            initOperandList.add((OPropertyExpression) result);
        }
        if (aBean.getDproperty() != null) {
            aBean.getDproperty().accept(this);
            lastOperand = (DPropertyExpression) result;
        }

        result = new DPropertyComposition(initOperandList, lastOperand);
    }

    public void visit(Dproperty.Difference aBean) {
        List<DPropertyExpression> propertyList = new ArrayList<DPropertyExpression>();

        for (Dproperty bean : aBean.getDproperty()) {
            bean.accept(this);
            propertyList.add((DPropertyExpression) result);
        }

        result = new DPropertyDifference(propertyList);
    }

    public void visit(Dproperty.Intersection aBean) {
        List<DPropertyExpression> propertyList = new ArrayList<DPropertyExpression>();

        for (Dproperty bean : aBean.getDproperty()) {
            bean.accept(this);
            propertyList.add((DPropertyExpression) result);
        }

        result = new DPropertyIntersection(propertyList);
    }

    public void visit(Dproperty.Transform aBean) {
        DPropertyExpression property = null;
        XmlDatatype datatype = null;

        if (aBean.getDproperty() != null) {
            aBean.getDproperty().accept(this);
            property = (DPropertyExpression) result;
        }
        if (aBean.getTo() != null) {
            datatype = XmlDatatype.valueOf(aBean.getTo().value());
        }

        result = new DPropertyTransform(property, datatype);
    }

    public void visit(Dproperty.Restrict aBean) {
        DPropertyExpression property = null;
        ClassExpression domain = null;
        gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype range = null;

        if (aBean.getDproperty() != null) {
            aBean.getDproperty().accept(this);
            property = (DPropertyExpression) result;
        }
        if (aBean.getDomain() != null) {
            aBean.getDomain().accept(this);
            domain = (ClassExpression) result;
        }
        if (aBean.getRange() != null) {
            aBean.getRange().accept(this);
            range = (gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype) result;
        }

        result = new DPropertyRestriction(property, domain, range);
    }

    public void visit(Dproperty.Restrict.Domain aBean) {
        if (aBean.getClazz() != null) {
            aBean.getClazz().accept(this);
        }
    }

    public void visit(Dproperty.Restrict.Range aBean) {
        if (aBean.getDatatype() != null) {
            aBean.getDatatype().accept(this);
        }
    }

    public void visit(Dproperty.Union aBean) {
        List<DPropertyExpression> propertyList = new ArrayList<DPropertyExpression>();

        for (Dproperty bean : aBean.getDproperty()) {
            bean.accept(this);
            propertyList.add((DPropertyExpression) result);
        }

        result = new DPropertyUnion(propertyList);
    }

    public void visit(Imapping aBean) {
        InstanceUri expr1 = null;
        InstanceUri expr2 = null;

        if (aBean.getExpr1() != null) {
            aBean.getExpr1().accept(this);
            expr1 = (InstanceUri) result;
        }
        if (aBean.getExpr2() != null) {
            aBean.getExpr2().accept(this);
            expr2 = (InstanceUri) result;
        }

        result = new InstanceMapping(aBean.getUri(), expr1, expr2, Relation.valueOf(aBean.getRelation()));
    }

    public void visit(Imapping.Expr1 aBean) {
        if (aBean.getIndividual() != null) {
            aBean.getIndividual().accept(this);
        }
    }

    public void visit(Imapping.Expr2 aBean) {
        if (aBean.getIndividual() != null) {
            aBean.getIndividual().accept(this);
        }
    }

    public void visit(Individual aBean) {
        if (aBean.getResource() != null) {
            aBean.getResource().accept(this);
            result = new InstanceUri((String) result);
        }
    }

    public void visit(Model aBean) {
        gr.tuc.music.sparqlrw.mapping.model.Ontology globalOntology = null;
        List<gr.tuc.music.sparqlrw.mapping.model.Ontology> localOntologyList = null;
        List<gr.tuc.music.sparqlrw.mapping.model.Mapping> mappingList = null;

        if (aBean.getGlobal() != null) {
            aBean.getGlobal().accept(this);
            globalOntology = (gr.tuc.music.sparqlrw.mapping.model.Ontology) result;
        }
        if (aBean.getLocals() != null) {
            aBean.getLocals().accept(this);
            localOntologyList = (List<gr.tuc.music.sparqlrw.mapping.model.Ontology>) (List<?>) result;
        }
        if (aBean.getMappings() != null) {
            aBean.getMappings().accept(this);
            mappingList = (List<gr.tuc.music.sparqlrw.mapping.model.Mapping>) (List<?>) result;
        }

        result = new MappingModel(Formalism.valueOf(aBean.getFormalism()), globalOntology, localOntologyList, mappingList);
    }

    public void visit(Model.Global aBean) {
        if (aBean.getOntology() != null) {
            aBean.getOntology().accept(this);
        }
    }

    public void visit(Model.Locals aBean) {
        List<gr.tuc.music.sparqlrw.mapping.model.Ontology> ontologyList = new ArrayList<gr.tuc.music.sparqlrw.mapping.model.Ontology>();

        for (Ontology bean : aBean.getOntology()) {
            bean.accept(this);
            ontologyList.add((gr.tuc.music.sparqlrw.mapping.model.Ontology) result);
        }

        result = ontologyList;
    }

    public void visit(Model.Mappings aBean) {
        List<Mapping> mappingList = new ArrayList<Mapping>();
        List<Object> objectList = aBean.getCmappingOrOpmappingOrDpmapping();

        for (Object mapping : objectList) {
            if (mapping instanceof Cmapping) {
                ((Cmapping) mapping).accept(this);
            } else if (mapping instanceof Opmapping) {
                ((Opmapping) mapping).accept(this);
            } else if (mapping instanceof Dpmapping) {
                ((Dpmapping) mapping).accept(this);
            } else {
                ((Imapping) mapping).accept(this);
            }

            mappingList.add((Mapping) result);
        }

        result = mappingList;
    }

    public void visit(Ontology aBean) {
        result = new gr.tuc.music.sparqlrw.mapping.model.Ontology(aBean.getUri(), aBean.getName(), aBean.getDescription(), aBean.getSchemaLocation());
    }

    public void visit(Opmapping aBean) {
        OPropertyExpression expr1 = null;
        OPropertyExpression expr2 = null;

        if (aBean.getExpr1() != null) {
            aBean.getExpr1().accept(this);
            expr1 = (OPropertyExpression) result;
        }
        if (aBean.getExpr2() != null) {
            aBean.getExpr2().accept(this);
            expr2 = (OPropertyExpression) result;
        }

        result = new OPropertyMapping(aBean.getUri(), expr1, expr2, Relation.valueOf(aBean.getRelation()));
    }

    public void visit(Opmapping.Expr1 aBean) {
        if (aBean.getOproperty() != null) {
            aBean.getOproperty().accept(this);
        }
    }

    public void visit(Opmapping.Expr2 aBean) {
        if (aBean.getOproperty() != null) {
            aBean.getOproperty().accept(this);
        }
    }

    public void visit(Oproperty aBean) {
        if (aBean.getResource() != null) {
            aBean.getResource().accept(this);
            result = new OPropertyUri((String) result);
        }
        if (aBean.getUnion() != null) {
            aBean.getUnion().accept(this);
        }
        if (aBean.getIntersection() != null) {
            aBean.getIntersection().accept(this);
        }
        if (aBean.getDifference() != null) {
            aBean.getDifference().accept(this);
        }
        if (aBean.getComposition() != null) {
            aBean.getComposition().accept(this);
        }
        if (aBean.getInverse() != null) {
            aBean.getInverse().accept(this);
        }
        if (aBean.getTransitive() != null) {
            aBean.getTransitive().accept(this);
        }
        if (aBean.getExistpred() != null) {
            aBean.getExistpred().accept(this);
        }
        if (aBean.getRestrict() != null) {
            aBean.getRestrict().accept(this);
        }
    }

    public void visit(Oproperty.Composition aBean) {
        List<OPropertyExpression> propertyList = new ArrayList<OPropertyExpression>();

        for (Oproperty bean : aBean.getOproperty()) {
            bean.accept(this);
            propertyList.add((OPropertyExpression) result);
        }

        result = new OPropertyComposition(propertyList);
    }

    public void visit(Oproperty.Difference aBean) {
        List<OPropertyExpression> propertyList = new ArrayList<OPropertyExpression>();

        for (Oproperty bean : aBean.getOproperty()) {
            bean.accept(this);
            propertyList.add((OPropertyExpression) result);
        }

        result = new OPropertyDifference(propertyList);
    }

    public void visit(Oproperty.Existpred aBean) {
        if (aBean.getOnOProperties() != null) {
            aBean.getOnOProperties().accept(this);
        }
        if (aBean.getOnDProperties() != null) {
            aBean.getOnDProperties().accept(this);
        }
    }

    public void visit(Oproperty.Existpred.OnDProperties aBean) {
        List<PropertyExpression> propertyList = new ArrayList<PropertyExpression>();
        List<PropertyExpression> operand1List = new ArrayList<PropertyExpression>();
        List<PropertyExpression> operand2List = new ArrayList<PropertyExpression>();
        Predicate predicate = null;

        for (Dproperty bean : aBean.getDproperty()) {
            bean.accept(this);
            propertyList.add((PropertyExpression) result);
        }
        if (aBean.getPredicate() != null) {
            aBean.getPredicate().accept(this);
            predicate = (Predicate) result;
        }

        operand1List.add(propertyList.get(0));
        operand2List.add(propertyList.get(1));

        result = new OPropertyExistPredRestriction(operand1List, operand2List, predicate);
    }

    public void visit(Oproperty.Existpred.OnOProperties aBean) {
        List<PropertyExpression> propertyList = new ArrayList<PropertyExpression>();
        List<PropertyExpression> operand1List = new ArrayList<PropertyExpression>();
        List<PropertyExpression> operand2List = new ArrayList<PropertyExpression>();
        Predicate predicate = null;

        for (Oproperty bean : aBean.getOproperty()) {
            bean.accept(this);
            propertyList.add((PropertyExpression) result);
        }
        if (aBean.getPredicate() != null) {
            aBean.getPredicate().accept(this);
            predicate = (Predicate) result;
        }

        operand1List.add(propertyList.get(0));
        operand2List.add(propertyList.get(1));

        result = new OPropertyExistPredRestriction(operand1List, operand2List, predicate);
    }

    public void visit(Oproperty.Intersection aBean) {
        List<OPropertyExpression> propertyList = new ArrayList<OPropertyExpression>();

        for (Oproperty bean : aBean.getOproperty()) {
            bean.accept(this);
            propertyList.add((OPropertyExpression) result);
        }

        result = new OPropertyIntersection(propertyList);
    }

    public void visit(Oproperty.Inverse aBean) {
        if (aBean.getOproperty() != null) {
            aBean.getOproperty().accept(this);
            result = new OPropertyInverse((OPropertyExpression) result);
        }
    }

    public void visit(Oproperty.Restrict aBean) {
        OPropertyExpression property = null;
        ClassExpression domain = null;
        ClassExpression range = null;

        if (aBean.getOproperty() != null) {
            aBean.getOproperty().accept(this);
            property = (OPropertyExpression) result;
        }
        if (aBean.getDomain() != null) {
            aBean.getDomain().accept(this);
            domain = (ClassExpression) result;
        }
        if (aBean.getRange() != null) {
            aBean.getRange().accept(this);
            range = (ClassExpression) result;
        }

        result = new OPropertyRestriction(property, domain, range);
    }

    public void visit(Oproperty.Restrict.Domain aBean) {
        if (aBean.getClazz() != null) {
            aBean.getClazz().accept(this);
        }
    }

    public void visit(Oproperty.Restrict.Range aBean) {
        if (aBean.getClazz() != null) {
            aBean.getClazz().accept(this);
        }
    }

    public void visit(Oproperty.Transitive aBean) {
        if (aBean.getOproperty() != null) {
            aBean.getOproperty().accept(this);
            result = new OPropertyTransitive((OPropertyExpression) result);
        }
    }

    public void visit(Oproperty.Union aBean) {
        List<OPropertyExpression> propertyList = new ArrayList<OPropertyExpression>();

        for (Oproperty bean : aBean.getOproperty()) {
            bean.accept(this);
            propertyList.add((OPropertyExpression) result);
        }

        result = new OPropertyUnion(propertyList);
    }

    public void visit(Resource aBean) {
        result = aBean.getUri();
    }

    public void visit(UPredicateType aBean) {
        gr.tuc.music.sparqlrw.mapping.model.value.Value value = null;

        if (aBean.getValue() != null) {
            aBean.getValue().accept(this);
            value = (gr.tuc.music.sparqlrw.mapping.model.value.Value) result;
        }

        result = new UnaryPredicate(Operator.valueOf(aBean.getOperator()), value);
    }

    public void visit(Value aBean) {
        if (aBean.getIndividual() != null) {
            aBean.getIndividual().accept(this);
        }
        if (aBean.getData() != null) {
            aBean.getData().accept(this);
        }
    }
}
