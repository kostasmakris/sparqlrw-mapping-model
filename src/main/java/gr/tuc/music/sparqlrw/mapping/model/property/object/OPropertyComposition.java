/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.property.object;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * A composition of object property expressions.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class OPropertyComposition extends OPropertyExpression {

    private final List<OPropertyExpression> operandList;

    /**
     * Creates an OPropertyComposition object using specified values.
     *
     * @param operandList A list of object property expressions.
     */
    public OPropertyComposition(List<OPropertyExpression> operandList) {
        this.operandList = new ArrayList<OPropertyExpression>(operandList);
    }

    public List<OPropertyExpression> getOperandList() {
        return operandList;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        for (OPropertyExpression expr : operandList) {
            resourceList.addAll(expr.listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.operandList != null ? this.operandList.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OPropertyComposition other = (OPropertyComposition) obj;
        if (this.operandList != other.operandList && (this.operandList == null || !this.operandList.equals(other.operandList))) {
            return false;
        }
        return true;
    }
}
