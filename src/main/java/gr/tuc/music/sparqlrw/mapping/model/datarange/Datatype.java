/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.datarange;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.concept.Quantifier;
import gr.tuc.music.sparqlrw.mapping.model.condition.Condition;

/**
 * A custom datatype.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class Datatype implements Quantifier {

    private final XmlDatatype base;
    private final Condition restriction;

    /**
     * Creates a Datatype object using specified values.
     *
     * @param base        A datatype.
     * @param restriction A value condition.
     */
    public Datatype(XmlDatatype base, Condition restriction) {
        this.base = base;
        this.restriction = restriction;
    }

    public XmlDatatype getBase() {
        return base;
    }

    public Condition getRestriction() {
        return restriction;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.base != null ? this.base.hashCode() : 0);
        hash = 89 * hash + (this.restriction != null ? this.restriction.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Datatype other = (Datatype) obj;
        if (this.base != other.base) {
            return false;
        }
        if (this.restriction != other.restriction && (this.restriction == null || !this.restriction.equals(other.restriction))) {
            return false;
        }
        return true;
    }
}
