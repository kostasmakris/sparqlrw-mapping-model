/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.property.datatype;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import java.util.ArrayList;
import java.util.List;

/**
 * A property path formed by a sequence of object property expressions followed
 * by a single datatype property expression.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class DPropertyComposition extends DPropertyExpression {

    private final List<OPropertyExpression> initOperandList;
    private final DPropertyExpression lastOperand;

    /**
     * Creates a DPropertyComposition object using specified values.
     *
     * @param initOperandList A list of object property expressions.
     * @param lastOperand     A datatype property expression.
     */
    public DPropertyComposition(List<OPropertyExpression> initOperandList, DPropertyExpression lastOperand) {
        this.initOperandList = new ArrayList<OPropertyExpression>(initOperandList);
        this.lastOperand = lastOperand;
    }

    public List<OPropertyExpression> getInitOperandList() {
        return initOperandList;
    }

    public DPropertyExpression getLastOperand() {
        return lastOperand;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        for (OPropertyExpression expr : initOperandList) {
            resourceList.addAll(expr.listResources());
        }

        if (lastOperand != null) {
            resourceList.addAll(lastOperand.listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.initOperandList != null ? this.initOperandList.hashCode() : 0);
        hash = 43 * hash + (this.lastOperand != null ? this.lastOperand.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DPropertyComposition other = (DPropertyComposition) obj;
        if (this.initOperandList != other.initOperandList && (this.initOperandList == null || !this.initOperandList.equals(other.initOperandList))) {
            return false;
        }
        if (this.lastOperand != other.lastOperand && (this.lastOperand == null || !this.lastOperand.equals(other.lastOperand))) {
            return false;
        }
        return true;
    }


}
