/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import java.util.ArrayList;
import java.util.List;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.slf4j.LoggerFactory;

/**
 * A placeholder for ontology information.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class Ontology {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(Ontology.class);

    private final String uri;
    private final String name;
    private final String description;
    private final String schemaLocation;
    private OWLOntology schema;

    /**
     * Creates an Ontology object using specified values.
     *
     * @param uri            The ontology uri.
     * @param name           The ontology name.
     * @param description    The ontology description.
     * @param schemaLocation The ontology schema location.
     */
    public Ontology(String uri, String name, String description, String schemaLocation) {
        this.uri = uri;
        this.name = name;
        this.description = description;
        this.schemaLocation = schemaLocation;

        if (schemaLocation != null) {
            try {
                OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

                // load ontology schema
                schema = manager.loadOntologyFromOntologyDocument(IRI.create(schemaLocation));

                // load directly imported ontologies and append their content to
                // the main ontology schema
                for (IRI iri : schema.getDirectImportsDocuments()) {
                    try {
                        OWLOntology ontology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(iri);
                        manager.addAxioms(schema, ontology.getAxioms());
                    } catch (OWLOntologyCreationException ex) {
                        log.warn("[Ontology: <" + iri.toString() + ">] schema failed to be loaded.", ex);
                    }
                }
            } catch (OWLOntologyCreationException ex) {
                log.warn("[Ontology: <" + uri + ">] schema failed to be loaded.", ex);
            }
        }
    }

    /**
     * Creates an Ontology object using specified values.
     *
     * @param uri         The ontology uri.
     * @param name        The ontology name.
     * @param description The ontology description.
     * @param schema      The ontology schema.
     */
    public Ontology(String uri, String name, String description, OWLOntology schema) {
        this.uri = uri;
        this.name = name;
        this.description = description;
        this.schemaLocation = null;
        this.schema = schema;
    }

    public String getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getSchemaLocation() {
        return schemaLocation;
    }

    public OWLOntology getSchema() {
        return schema;
    }

    public void setSchema(OWLOntology schema) {
        this.schema = schema;
    }

    /**
     * Lists the resources appearing in the ontology schema.
     *
     * @return A list of resources.
     */
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        if (schema == null) {
            log.debug("[Ontology: <" + uri + ">] schema has not been loaded.");
            return resourceList;
        }

        // add classes
        for (OWLClass resource : schema.getClassesInSignature()) {
            resourceList.add(new ClassUri(resource.getIRI().toString()));
        }

        // add object properties
        for (OWLObjectProperty resource : schema.getObjectPropertiesInSignature()) {
            resourceList.add(new OPropertyUri(resource.getIRI().toString()));
        }

        // add datatype properties
        for (OWLDataProperty resource : schema.getDataPropertiesInSignature()) {
            resourceList.add(new DPropertyUri(resource.getIRI().toString()));
        }

        // add individuals
        for (OWLNamedIndividual resource : schema.getIndividualsInSignature()) {
            resourceList.add(new InstanceUri(resource.getIRI().toString()));
        }

        return resourceList;
    }

    /**
     * Checks if the ontology contains a resource having a specific uri.
     *
     * @param uri A uri.
     * @return True if the ontology contains a resource with the specified uri,
     *         otherwise false.
     */
    public boolean contains(String uri) {
        boolean result = false;

        if (schema == null) {
            log.debug("[Ontology: <" + uri + ">] schema has not been loaded.");
            return result;
        }

        if (uri == null) {
            log.debug("Input parameter is null.");
            return result;
        }

        return schema.containsEntityInSignature(IRI.create(uri));
    }

    /**
     * Checks if the ontology contains a specific resource.
     *
     * @param resource A resource.
     * @return True if the ontology contains the specified resource, otherwise
     *         false.
     */
    public boolean contains(Resource resource) {
        boolean result = false;

        if (schema == null) {
            log.debug("[Ontology: <" + uri + ">] schema has not been loaded.");
            return result;
        }

        if (resource == null) {
            log.debug("Input parameter is null.");
            return result;
        }

        return schema.containsEntityInSignature(IRI.create(resource.getUri()));
    }

    /**
     * Checks if the ontology contains a specific class.
     *
     * @param resource A resource.
     * @return True if the ontology contains the specified class, otherwise
     *         false.
     */
    public boolean containsClass(Resource resource) {
        boolean result = false;

        if (schema == null) {
            log.debug("[Ontology: <" + uri + ">] schema has not been loaded.");
            return result;
        }

        if (resource == null) {
            log.debug("Input parameter is null.");
            return result;
        }

        if (schema.containsClassInSignature(IRI.create(resource.getUri()))) {
            result = true;
        }

        return result;
    }

    /**
     * Checks if the ontology contains a specific object property.
     *
     * @param resource A resource.
     * @return True if the ontology contains the specified object property,
     *         otherwise false.
     */
    public boolean containsOProperty(Resource resource) {
        boolean result = false;

        if (schema == null) {
            log.debug("[Ontology: <" + uri + ">] schema has not been loaded.");
            return result;
        }

        if (resource == null) {
            log.debug("Input parameter is null.");
            return result;
        }

        if (schema.containsObjectPropertyInSignature(IRI.create(resource.getUri()))) {
            result = true;
        }

        return result;
    }

    /**
     * Checks if the ontology contains a specific datatype property.
     *
     * @param resource A resource.
     * @return True if the ontology contains the specified datatype property,
     *         otherwise false.
     */
    public boolean containsDProperty(Resource resource) {
        boolean result = false;

        if (schema == null) {
            log.debug("[Ontology: <" + uri + ">] schema has not been loaded.");
            return result;
        }

        if (resource == null) {
            log.debug("Input parameter is null.");
            return result;
        }

        if (schema.containsDataPropertyInSignature(IRI.create(resource.getUri()))) {
            result = true;
        }

        return result;
    }

    /**
     * Checks if the ontology contains a specific class instance.
     *
     * @param resource A resource.
     * @return True if the ontology contains the specified class instance,
     *         otherwise false.
     */
    public boolean containsInstance(Resource resource) {
        boolean result = false;

        if (schema == null) {
            log.debug("[Ontology: <" + uri + ">] schema has not been loaded.");
            return result;
        }

        if (resource == null) {
            log.debug("Input parameter is null.");
            return result;
        }

        if (schema.containsIndividualInSignature(IRI.create(resource.getUri()))) {
            result = true;
        }

        return result;
    }

    public <T> T accept(MappingModelVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
