/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import gr.tuc.music.sparqlrw.mapping.model.concept.ClassCardinalityRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassDifference;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassEnumeration;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExistQuantification;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassExpression;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassIntersection;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassMapping;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUnion;
import gr.tuc.music.sparqlrw.mapping.model.concept.ClassUri;
import gr.tuc.music.sparqlrw.mapping.model.condition.AndCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.BasicCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.Condition;
import gr.tuc.music.sparqlrw.mapping.model.condition.ConditionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.condition.NotCondition;
import gr.tuc.music.sparqlrw.mapping.model.condition.OrCondition;
import gr.tuc.music.sparqlrw.mapping.model.datarange.Datatype;
import gr.tuc.music.sparqlrw.mapping.model.datarange.XmlDatatype;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceMapping;
import gr.tuc.music.sparqlrw.mapping.model.instance.InstanceUri;
import gr.tuc.music.sparqlrw.mapping.model.predicate.Operator;
import gr.tuc.music.sparqlrw.mapping.model.predicate.UnaryPredicate;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyTransform;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.datatype.DPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyComposition;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyDifference;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExistPredRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyExpression;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyIntersection;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyInverse;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyMapping;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyRestriction;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyTransitive;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUnion;
import gr.tuc.music.sparqlrw.mapping.model.property.object.OPropertyUri;
import gr.tuc.music.sparqlrw.mapping.model.value.DataValue;
import gr.tuc.music.sparqlrw.mapping.model.value.Value;
import gr.tuc.music.sparqlrw.mapping.model.value.ValueVisitor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLPropertyExpression;
import org.semanticweb.owlapi.vocab.OWL2Datatype;
import org.slf4j.LoggerFactory;

/**
 * Converts an input SPARQL-RW mapping model (or a single mapping) to an OWL
 * ontology (implementing post-order traversal) using the OWL-API.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class OWLConverter implements MappingVisitor<Void>, ExpressionVisitor<Object>, ConditionVisitor<Object, Void>, ValueVisitor<Object> {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(OWLConverter.class);

    private OWLOntology ontology;
    private OWLOntologyManager manager;
    private OWLDataFactory factory;

    /**
     * Converts an input SPARQL-RW mapping model to an OWL ontology.
     *
     * @param model A SPARQL-RW mapping model object.
     * @return An OWL ontology.
     */
    public OWLOntology convert(MappingModel model) {

        if (model == null) {
            log.warn("Input parameter cannot be null.");
            return null;
        }

        manager = OWLManager.createOWLOntologyManager();
        factory = manager.getOWLDataFactory();

        try {
            ontology = manager.createOntology();
        } catch (OWLOntologyCreationException ex) {
            log.warn("Mapping model failed to be converted.", ex);
            return null;
        }

        // start visiting the mapping model nodes

        // browse the mapping list
        for (Mapping mapping : model.getMappingList()) {
            mapping.accept(this);
        }

        return ontology;
    }

    public Void visit(ClassMapping o) {
        OWLClassExpression expr1 = null;
        OWLClassExpression expr2 = null;

        // create the first expression
        if (o.getExpr1() != null) {
            expr1 = (OWLClassExpression) o.getExpr1().accept(this);
        }

        // create the second expression
        if (o.getExpr2() != null) {
            expr2 = (OWLClassExpression) o.getExpr2().accept(this);
        }

        // set the relation
        if ((expr1 != null) && (expr2 != null)) {
            if (o.getRelation() == Relation.EQUIVALENT) {
                OWLAxiom axiom = factory.getOWLEquivalentClassesAxiom(expr1, expr2);
                manager.addAxiom(ontology, axiom);
            } else if (o.getRelation() == Relation.SUBSUMED) {
                OWLAxiom axiom = factory.getOWLSubClassOfAxiom(expr1, expr2);
                manager.addAxiom(ontology, axiom);
            } else if (o.getRelation() == Relation.SUBSUMES) {
                OWLAxiom axiom = factory.getOWLSubClassOfAxiom(expr2, expr1);
                manager.addAxiom(ontology, axiom);
            }
        }

        return null;
    }

    public Void visit(OPropertyMapping o) {
        OWLObjectPropertyExpression expr1 = null;
        OWLObjectPropertyExpression expr2 = null;

        // create the first expression
        if (o.getExpr1() != null) {
            expr1 = (OWLObjectPropertyExpression) o.getExpr1().accept(this);
        }

        // create the second expression
        if (o.getExpr2() != null) {
            expr2 = (OWLObjectPropertyExpression) o.getExpr2().accept(this);
        }

        // set the relation
        if ((expr1 != null) && (expr2 != null)) {
            if (o.getRelation() == Relation.EQUIVALENT) {
                OWLAxiom axiom = factory.getOWLEquivalentObjectPropertiesAxiom(expr1, expr2);
                manager.addAxiom(ontology, axiom);
            } else if (o.getRelation() == Relation.SUBSUMED) {
                OWLAxiom axiom = factory.getOWLSubObjectPropertyOfAxiom(expr1, expr2);
                manager.addAxiom(ontology, axiom);
            } else if (o.getRelation() == Relation.SUBSUMES) {
                OWLAxiom axiom = factory.getOWLSubObjectPropertyOfAxiom(expr2, expr1);
                manager.addAxiom(ontology, axiom);
            }
        }

        return null;
    }

    public Void visit(DPropertyMapping o) {
        OWLDataPropertyExpression expr1 = null;
        OWLDataPropertyExpression expr2 = null;

        // create the first expression
        if (o.getExpr1() != null) {
            expr1 = (OWLDataPropertyExpression) o.getExpr1().accept(this);
        }

        // create the second expression
        if (o.getExpr2() != null) {
            expr2 = (OWLDataPropertyExpression) o.getExpr2().accept(this);
        }

        // set the relation
        if ((expr1 != null) && (expr2 != null)) {
            if (o.getRelation() == Relation.EQUIVALENT) {
                OWLAxiom axiom = factory.getOWLEquivalentDataPropertiesAxiom(expr1, expr2);
                manager.addAxiom(ontology, axiom);
            } else if (o.getRelation() == Relation.SUBSUMED) {
                OWLAxiom axiom = factory.getOWLSubDataPropertyOfAxiom(expr1, expr2);
                manager.addAxiom(ontology, axiom);
            } else if (o.getRelation() == Relation.SUBSUMES) {
                OWLAxiom axiom = factory.getOWLSubDataPropertyOfAxiom(expr2, expr1);
                manager.addAxiom(ontology, axiom);
            }
        }

        return null;
    }

    public Void visit(InstanceMapping o) {
        OWLIndividual instance1 = null;
        OWLIndividual instance2 = null;

        // create the first class instance
        if (o.getExpr1() != null) {
            instance1 = (OWLIndividual) o.getExpr1().accept((ExpressionVisitor) this);
        }

        // create the second class instance
        if (o.getExpr2() != null) {
            instance2 = (OWLIndividual) o.getExpr2().accept((ExpressionVisitor) this);
        }

        // set the relation
        if ((instance1 != null) && (instance2 != null) && (o.getRelation() == Relation.EQUIVALENT)) {
            instance1.getSameIndividuals(ontology).add(instance2);
        }

        return null;
    }

    public Object visit(ClassUri o) {
        if (o.getUri() != null) {
            return factory.getOWLClass(IRI.create(o.getUri()));
        }

        return null;
    }

    public Object visit(ClassUnion o) {
        Set<OWLClassExpression> exprSet = new HashSet<OWLClassExpression>();

        for (ClassExpression expr : o.getOperandList()) {
            OWLClassExpression temp = (OWLClassExpression) expr.accept(this);

            if (temp != null) {
                exprSet.add(temp);
            }
        }

        // a class union should have more than 2 operands
        if (exprSet.size() > 1) {
            return factory.getOWLObjectUnionOf(exprSet);
        }

        return null;
    }

    public Object visit(ClassIntersection o) {
        Set<OWLClassExpression> exprSet = new HashSet<OWLClassExpression>();

        for (ClassExpression expr : o.getOperandList()) {
            OWLClassExpression temp = (OWLClassExpression) expr.accept(this);

            if (temp != null) {
                exprSet.add(temp);
            }
        }

        // a class intersection should have more than 2 operands
        if (exprSet.size() > 1) {
            return factory.getOWLObjectIntersectionOf(exprSet);
        }

        return null;
    }

    public Object visit(ClassDifference o) {
        List<OWLClassExpression> exprSet = new ArrayList<OWLClassExpression>();

        for (ClassExpression expr : o.getOperandList()) {
            OWLClassExpression temp = (OWLClassExpression) expr.accept(this);

            if (temp != null) {
                exprSet.add(temp);
            }
        }

        // This operation is not directly supported by any OWL profile.
        // Use the rule: A - B = A AND (NOT B)

        while (exprSet.size() > 1) {
            OWLClassExpression expr1 = exprSet.remove(0);
            OWLClassExpression expr2 = factory.getOWLObjectComplementOf(exprSet.remove(0));

            OWLClassExpression intersect = factory.getOWLObjectIntersectionOf(expr1, expr2);
            exprSet.add(0, intersect);

            if (exprSet.size() == 1) {
                return intersect;
            }
        }

        return null;
    }

    public Object visit(ClassEnumeration o) {
        Set<OWLIndividual> instanceSet = new HashSet<OWLIndividual>();

        for (InstanceUri expr : o.getOperandList()) {
            OWLIndividual temp = (OWLIndividual) expr.accept((ExpressionVisitor) this);

            if (temp != null) {
                instanceSet.add(temp);
            }
        }

        // an enumerated class should have more than 2 operands
        if (instanceSet.size() > 1) {
            return factory.getOWLObjectOneOf(instanceSet);
        }

        return null;
    }

    public Object visit(ClassCardinalityRestriction o) {
        OWLPropertyExpression propertyExpr = null;
        OWLObject quantifier = null;
        int cardinality = -1;

        // create the property where the restriction is applied
        if (o.getProperty() != null) {
            propertyExpr = (OWLPropertyExpression) o.getProperty().accept(this);
        }

        if (propertyExpr == null) {
            return null;
        }

        // create the property quantifier (optional)
        if (o.getQuantifier() != null) {
            quantifier = (OWLObject) o.getQuantifier().accept(this);
        }

        if (o.getCardinality() != null) {
            UnaryPredicate predicate = o.getCardinality();
            Operator operator = predicate.getOperator();
            Value value = predicate.getValue();

            // get cardinality value
            if (value != null) {
                OWLLiteral literal = (OWLLiteral) value.accept((ValueVisitor) this);

                if (literal != null) {
                    cardinality = (literal).parseInteger();
                }
            }

            if (cardinality == -1) {
                return null;
            }

            // decrease cardinality value since OWL supports only MIN, MAX, EQUAL
            if ((operator == Operator.GREATER) || operator == Operator.LESS) {
                cardinality = cardinality - 1;
            }

            // create the cardinality restriction
            if (operator == Operator.EQUAL) {
                if (quantifier == null) {
                    if (propertyExpr.isObjectPropertyExpression()) {
                        return factory.getOWLObjectExactCardinality(cardinality, (OWLObjectPropertyExpression) propertyExpr);
                    } else if (propertyExpr.isDataPropertyExpression()) {
                        return factory.getOWLDataExactCardinality(cardinality, (OWLDataPropertyExpression) propertyExpr);
                    }
                } else {
                    if (propertyExpr.isObjectPropertyExpression()) {
                        return factory.getOWLObjectExactCardinality(cardinality, (OWLObjectPropertyExpression) propertyExpr, (OWLClassExpression) quantifier);
                    } else if (propertyExpr.isDataPropertyExpression()) {
                        return factory.getOWLDataExactCardinality(cardinality, (OWLDataPropertyExpression) propertyExpr, (OWLDataRange) quantifier);
                    }
                }
            } else if ((operator == Operator.GREATER) || (operator == Operator.GREATER_EQUAL)) {
                if (quantifier == null) {
                    if (propertyExpr.isObjectPropertyExpression()) {
                        return factory.getOWLObjectMinCardinality(cardinality, (OWLObjectPropertyExpression) propertyExpr);
                    } else if (propertyExpr.isDataPropertyExpression()) {
                        return factory.getOWLDataMinCardinality(cardinality, (OWLDataPropertyExpression) propertyExpr);
                    }
                } else {
                    if (propertyExpr.isObjectPropertyExpression()) {
                        return factory.getOWLObjectMinCardinality(cardinality, (OWLObjectPropertyExpression) propertyExpr, (OWLClassExpression) quantifier);
                    } else if (propertyExpr.isDataPropertyExpression()) {
                        return factory.getOWLDataMinCardinality(cardinality, (OWLDataPropertyExpression) propertyExpr, (OWLDataRange) quantifier);
                    }
                }
            } else if ((operator == Operator.LESS) || (operator == Operator.LESS_EQUAL)) {
                if (quantifier == null) {
                    if (propertyExpr.isObjectPropertyExpression()) {
                        return factory.getOWLObjectMaxCardinality(cardinality, (OWLObjectPropertyExpression) propertyExpr);
                    } else if (propertyExpr.isDataPropertyExpression()) {
                        return factory.getOWLDataMaxCardinality(cardinality, (OWLDataPropertyExpression) propertyExpr);
                    }
                } else {
                    if (propertyExpr.isObjectPropertyExpression()) {
                        return factory.getOWLObjectMaxCardinality(cardinality, (OWLObjectPropertyExpression) propertyExpr, (OWLClassExpression) quantifier);
                    } else if (propertyExpr.isDataPropertyExpression()) {
                        return factory.getOWLDataMaxCardinality(cardinality, (OWLDataPropertyExpression) propertyExpr, (OWLDataRange) quantifier);
                    }
                }
            }
        }

        return null;
    }

    public Object visit(ClassExistPredRestriction o) {
        OWLPropertyExpression propertyExpr;

        // Existential predicate restriction is supported only for a single property.

        if ((o.getOperandList() == null) || (o.getOperandList().isEmpty()) || (o.getOperandList().size() > 1)) {
            return null;
        }

        propertyExpr = (OWLPropertyExpression) o.getOperandList().get(0).accept(this);
        if (propertyExpr == null) {
            return null;
        }

        if ((o.getPredicate() != null) && (o.getPredicate() instanceof UnaryPredicate)) {
            UnaryPredicate predicate = (UnaryPredicate) o.getPredicate();
            Operator operator = predicate.getOperator();
            OWLObject value = null;

            // get value
            if (predicate.getValue() != null) {
                value = (OWLObject) predicate.getValue().accept((ValueVisitor) this);
            }

            // create the restriction
            if (operator == Operator.EQUAL) {
                if (propertyExpr.isObjectPropertyExpression()) {
                    return factory.getOWLObjectHasValue((OWLObjectPropertyExpression) propertyExpr, (OWLIndividual) value);
                } else if (propertyExpr.isDataPropertyExpression()) {
                    return factory.getOWLDataHasValue((OWLDataPropertyExpression) propertyExpr, (OWLLiteral) value);
                }
            }
        }

        return null;
    }

    public Object visit(ClassExistQuantification o) {
        OWLPropertyExpression propertyExpr = null;
        OWLObject quantifier = null;

        // create the property where the restriction is applied
        if (o.getProperty() != null) {
            propertyExpr = (OWLPropertyExpression) o.getProperty().accept(this);
        }

        // create the property quantifier (either Class or Datatype)
        if (o.getQuantifier() != null) {
            quantifier = (OWLObject) o.getQuantifier().accept(this);
        }

        if ((propertyExpr != null) && (quantifier != null)) {
            if (propertyExpr.isObjectPropertyExpression()) {
                return factory.getOWLObjectSomeValuesFrom((OWLObjectPropertyExpression) propertyExpr, (OWLClassExpression) quantifier);
            } else if (propertyExpr.isDataPropertyExpression()) {
                return factory.getOWLDataSomeValuesFrom((OWLDataPropertyExpression) propertyExpr, (OWLDataRange) quantifier);
            }
        }

        return null;
    }

    public Object visit(OPropertyUri o) {
        if (o.getUri() != null) {
            return factory.getOWLObjectProperty(IRI.create(o.getUri()));
        }

        return null;
    }

    public Object visit(OPropertyUnion o) {
        // This operation is not supported by any OWL profile.
        return null;
    }

    public Object visit(OPropertyIntersection o) {
        // This operation is not supported by any OWL profile.
        return null;
    }

    public Object visit(OPropertyDifference o) {
        // This operation is not supported by any OWL profile.
        return null;
    }

    public Object visit(OPropertyComposition o) {
        List<OWLObjectPropertyExpression> exprList = new ArrayList<OWLObjectPropertyExpression>();

        // This operation is not directly supported by OWL2 (Axiom).
        // Handled as: A COMP B => BLANK_NODE SUBPROPERTY_OF (Chain(A, B))

        for (OPropertyExpression property : o.getOperandList()) {
            OWLObjectPropertyExpression temp = (OWLObjectPropertyExpression) property.accept(this);

            if (temp != null) {
                exprList.add(temp);
            }
        }

        if (!exprList.isEmpty()) {
            OWLObjectPropertyExpression temp = factory.getOWLObjectProperty(IRI.generateDocumentIRI());

            OWLAxiom axiom = factory.getOWLSubPropertyChainOfAxiom(exprList, temp);
            manager.addAxiom(ontology, axiom);

            return temp;
        }

        return null;
    }

    public Object visit(OPropertyInverse o) {
        if (o.getOperand() != null) {
            OWLObjectPropertyExpression propertyExpr = (OWLObjectPropertyExpression) o.getOperand().accept(this);
            if (propertyExpr != null) {
                return factory.getOWLObjectInverseOf(propertyExpr);
            }
        }

        return null;
    }

    public Object visit(OPropertyTransitive o) {
        if (o.getOperand() != null) {
            OWLObjectPropertyExpression propertyExpr = (OWLObjectPropertyExpression) o.getOperand().accept(this);
            if (propertyExpr != null) {
                // create an unnamed object property (blank node)
                OWLObjectPropertyExpression unnamedPropertyExpr = factory.getOWLObjectProperty(IRI.generateDocumentIRI());

                // set the unnamed property to be the equivalent of the previously visited object property
                OWLAxiom axiom = factory.getOWLEquivalentObjectPropertiesAxiom(propertyExpr, unnamedPropertyExpr);
                manager.addAxiom(ontology, axiom);

                // set the unnamed property to be transitive
                axiom = factory.getOWLTransitiveObjectPropertyAxiom(unnamedPropertyExpr);
                manager.addAxiom(ontology, axiom);

                return unnamedPropertyExpr;
            }
        }

        return null;
    }

    public Object visit(OPropertyRestriction o) {
        OWLObjectPropertyExpression propertyExpr = null;

        // create the object property
        if (o.getProperty() != null) {
            propertyExpr = (OWLObjectPropertyExpression) o.getProperty().accept(this);
        }

        if (propertyExpr != null) {
            // create the domain restriction
            if (o.getDomain() != null) {
                OWLClassExpression domain = (OWLClassExpression) o.getDomain().accept(this);

                if (domain != null) {
                    OWLAxiom axiom = factory.getOWLObjectPropertyDomainAxiom(propertyExpr, domain);
                    manager.addAxiom(ontology, axiom);
                }
            }

            // create the range restriction
            if (o.getRange() != null) {
                OWLClassExpression range = (OWLClassExpression) o.getRange().accept(this);

                if (range != null) {
                    OWLAxiom axiom = factory.getOWLObjectPropertyRangeAxiom(propertyExpr, range);
                    manager.addAxiom(ontology, axiom);
                }
            }

            return propertyExpr;
        }

        return null;
    }

    public Object visit(OPropertyExistPredRestriction o) {
        // This operation is not supported by any OWL profile.
        return null;
    }

    public Object visit(DPropertyUri o) {
        if (o.getUri() != null) {
            return factory.getOWLDataProperty(IRI.create(o.getUri()));
        }

        return null;
    }

    public Object visit(DPropertyUnion o) {
        // This operation is not supported by any OWL profile.
        return null;
    }

    public Object visit(DPropertyIntersection o) {
        // This operation is not supported by any OWL profile.
        return null;
    }

    public Object visit(DPropertyDifference o) {
        // This operation is not supported by any OWL profile.
        return null;
    }

    public Object visit(DPropertyComposition o) {
        // This operation is not supported by any OWL profile.
        return null;
    }

    public Object visit(DPropertyTransform o) {
        OWLDatatype base;
        OWLDataPropertyExpression propertyExpr = null;

        // create the datatype property
        if (o.getProperty() != null) {
            propertyExpr = (OWLDataPropertyExpression) o.getProperty().accept(this);
        }

        if (o.getDatatype() != null) {
            XmlDatatype datatype = o.getDatatype();

            if (datatype == XmlDatatype.BOOLEAN) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_BOOLEAN.getIRI());
            } else if (datatype == XmlDatatype.DATETIME) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_DATE_TIME.getIRI());
            } else if (datatype == XmlDatatype.DECIMAL) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_DECIMAL.getIRI());
            } else if (datatype == XmlDatatype.DOUBLE) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_DOUBLE.getIRI());
            } else if (datatype == XmlDatatype.FLOAT) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_FLOAT.getIRI());
            } else if (datatype == XmlDatatype.INTEGER) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_INTEGER.getIRI());
            } else {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_STRING.getIRI());
            }

            OWLAxiom axiom = factory.getOWLDataPropertyRangeAxiom(propertyExpr, base);
            manager.addAxiom(ontology, axiom);
        }

        return null;
    }

    public Object visit(DPropertyRestriction o) {
        OWLDataPropertyExpression propertyExpr = null;

        // create the datatype property
        if (o.getProperty() != null) {
            propertyExpr = (OWLDataPropertyExpression) o.getProperty().accept(this);
        }

        if (propertyExpr != null) {
            // create the domain restriction
            if (o.getDomain() != null) {
                OWLClassExpression domain = (OWLClassExpression) o.getDomain().accept(this);

                if (domain != null) {
                    OWLAxiom axiom = factory.getOWLDataPropertyDomainAxiom(propertyExpr, domain);
                    manager.addAxiom(ontology, axiom);
                }
            }

            // create the range restriction
            if (o.getRange() != null) {
                OWLDataRange range = (OWLDataRange) o.getRange().accept(this);

                if (range != null) {
                    OWLAxiom axiom = factory.getOWLDataPropertyRangeAxiom(propertyExpr, range);
                    manager.addAxiom(ontology, axiom);
                }
            }

            return propertyExpr;
        }

        return null;
    }

    public Object visit(InstanceUri o) {
        if (o.getUri() != null) {
            return factory.getOWLNamedIndividual(IRI.create(o.getUri()));
        }

        return null;
    }

    public Object visit(Datatype o) {
        OWLDatatype base = null;
        OWLDataRange restriction = null;

        // construct the basic data range
        if (o.getBase() != null) {
            XmlDatatype datatype = o.getBase();

            if (datatype == XmlDatatype.BOOLEAN) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_BOOLEAN.getIRI());
            } else if (datatype == XmlDatatype.DATETIME) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_DATE_TIME.getIRI());
            } else if (datatype == XmlDatatype.DECIMAL) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_DECIMAL.getIRI());
            } else if (datatype == XmlDatatype.DOUBLE) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_DOUBLE.getIRI());
            } else if (datatype == XmlDatatype.FLOAT) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_FLOAT.getIRI());
            } else if (datatype == XmlDatatype.INTEGER) {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_INTEGER.getIRI());
            } else {
                base = factory.getOWLDatatype(OWL2Datatype.XSD_STRING.getIRI());
            }
        }

        // construct the data range restriction
        if (o.getRestriction() != null) {
            restriction = (OWLDataRange) o.getRestriction().accept(this, null);
        }

        if ((base != null) && (restriction != null)) {
            return factory.getOWLDataIntersectionOf(base, restriction);
        } else {
            if (base != null) {
                return base;
            }

            if (restriction != null) {
                return restriction;
            }
        }

        return null;
    }

    public Object visit(BasicCondition c, Void o) {
        OWLLiteral value = null;

        if (c.getRestriction() == null) {
            return null;
        }

        if (c.getRestriction().getValue() != null) {
            value = (OWLLiteral) c.getRestriction().getValue().accept((ValueVisitor) this);
        }

        if (value != null) {
            Operator operator = c.getRestriction().getOperator();

            if (operator == Operator.EQUAL) {
                return factory.getOWLDataOneOf(value);
            } else if (operator == Operator.NOT_EQUAL) {
                return factory.getOWLDataComplementOf(factory.getOWLDataOneOf(value));
            } else if (operator == Operator.GREATER) {
                if (value.isInteger()) {
                    return factory.getOWLDatatypeMinExclusiveRestriction(value.parseInteger());
                } else if (value.isDouble()) {
                    return factory.getOWLDatatypeMinExclusiveRestriction(value.parseDouble());
                }
            } else if (operator == Operator.GREATER_EQUAL) {
                if (value.isInteger()) {
                    return factory.getOWLDatatypeMinInclusiveRestriction(value.parseInteger());
                } else if (value.isDouble()) {
                    return factory.getOWLDatatypeMinInclusiveRestriction(value.parseDouble());
                }
            } else if (operator == Operator.LESS) {
                if (value.isInteger()) {
                    return factory.getOWLDatatypeMaxExclusiveRestriction(value.parseInteger());
                } else if (value.isDouble()) {
                    return factory.getOWLDatatypeMaxExclusiveRestriction(value.parseDouble());
                }
            } else if (operator == Operator.LESS_EQUAL) {
                if (value.isInteger()) {
                    return factory.getOWLDatatypeMaxInclusiveRestriction(value.parseInteger());
                } else if (value.isDouble()) {
                    return factory.getOWLDatatypeMaxInclusiveRestriction(value.parseDouble());
                }
            }
        }

        return null;
    }

    public Object visit(AndCondition c, Void o) {
        Set<OWLDataRange> dataRangeSet = new HashSet<OWLDataRange>();

        for (Condition condition : c.getOperandList()) {
            OWLDataRange dataRange = (OWLDataRange) condition.accept(this, o);

            if (dataRange != null) {
                dataRangeSet.add(dataRange);
            }
        }

        if (dataRangeSet.size() > 1) {
            return factory.getOWLDataIntersectionOf(dataRangeSet);
        }

        return null;
    }

    public Object visit(OrCondition c, Void o) {
        Set<OWLDataRange> dataRangeSet = new HashSet<OWLDataRange>();

        for (Condition condition : c.getOperandList()) {
            OWLDataRange dataRange = (OWLDataRange) condition.accept(this, o);

            if (dataRange != null) {
                dataRangeSet.add(dataRange);
            }
        }

        if (dataRangeSet.size() > 1) {
            return factory.getOWLDataUnionOf(dataRangeSet);
        }

        return null;
    }

    public Object visit(NotCondition c, Void o) {

        if (c.getOperand() != null) {
            OWLDataRange dataRange = (OWLDataRange) c.getOperand().accept(this, o);

            if (dataRange != null) {
                return factory.getOWLDataComplementOf(dataRange);
            }
        }

        return null;
    }

    public Object visit(DataValue v) {
        if (v.getDatatype() == XmlDatatype.BOOLEAN) {
            return factory.getOWLLiteral(Boolean.parseBoolean(v.getValue()));
        } else if (v.getDatatype() == XmlDatatype.DATETIME) {
            return factory.getOWLLiteral(v.getValue());
        } else if (v.getDatatype() == XmlDatatype.DECIMAL) {
            return factory.getOWLLiteral(Float.parseFloat(v.getValue()));
        } else if (v.getDatatype() == XmlDatatype.DOUBLE) {
            return factory.getOWLLiteral(Double.parseDouble(v.getValue()));
        } else if (v.getDatatype() == XmlDatatype.FLOAT) {
            return factory.getOWLLiteral(Float.parseFloat(v.getValue()));
        } else if (v.getDatatype() == XmlDatatype.INTEGER) {
            return factory.getOWLLiteral(Integer.parseInt(v.getValue()));
        } else if (v.getDatatype() == XmlDatatype.STRING) {
            return factory.getOWLLiteral(v.getValue());
        } else {
            return factory.getOWLLiteral(v.getValue());
        }
    }
}
