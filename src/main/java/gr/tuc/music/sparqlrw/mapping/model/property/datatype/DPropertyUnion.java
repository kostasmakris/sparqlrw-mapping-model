/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model.property.datatype;

import gr.tuc.music.sparqlrw.mapping.model.ExpressionVisitor;
import gr.tuc.music.sparqlrw.mapping.model.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * A union of datatype property expressions.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class DPropertyUnion extends DPropertyExpression {

    private final List<DPropertyExpression> operandList;

    /**
     * Creates a DPropertyUnion object using specified values.
     *
     * @param operandList A list of datatype property expressions.
     */
    public DPropertyUnion(List<DPropertyExpression> operandList) {
        this.operandList = new ArrayList<DPropertyExpression>(operandList);
    }

    public List<DPropertyExpression> getOperandList() {
        return operandList;
    }

    @Override
    public List<Resource> listResources() {
        List<Resource> resourceList = new ArrayList<Resource>();

        for (DPropertyExpression expr : operandList) {
            resourceList.addAll(expr.listResources());
        }

        return resourceList;
    }

    @Override
    public <T> T accept(ExpressionVisitor<T> visitor) {
        return visitor.visit(this);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.operandList != null ? this.operandList.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DPropertyUnion other = (DPropertyUnion) obj;
        if (this.operandList != other.operandList && (this.operandList == null || !this.operandList.equals(other.operandList))) {
            return false;
        }
        return true;
    }
}
