/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import java.io.File;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class SerializerTest extends TestCase {

    public SerializerTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of serialize method, of class Serializer.
     */
    public void testSerialize() {
        System.out.println("serialize");

        Serializer serializer = new Serializer();
        Deserializer deserializer = new Deserializer();

        File gavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/mappings/gav.xml"));
        File lavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/mappings/lav.xml"));
        File glavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/mappings/glav.xml"));

        MappingModel model = deserializer.deserialize(gavMappings);
        serializer.serialize(model);

        model = deserializer.deserialize(lavMappings);
        serializer.serialize(model);

        model = deserializer.deserialize(glavMappings);
        serializer.serialize(model);
    }
}
