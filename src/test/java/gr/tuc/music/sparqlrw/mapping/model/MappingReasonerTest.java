/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import java.io.File;
import java.util.List;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class MappingReasonerTest extends TestCase {

    public MappingReasonerTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of computeInfMappings method, of class MappingReasoner.
     */
    public void testComputeInfMappings() {
        System.out.println("computeInfMappings");

        Deserializer deserializer = new Deserializer();

        File mediator = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/ontologies/mediator.owl"));
        File source1 = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/ontologies/source1.owl"));
        File source2 = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/ontologies/source2.owl"));

        File gavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/mappings/gav.xml"));
        File lavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/mappings/lav.modified.xml"));
        File glavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/mappings/glav.xml"));

        OWLOntology globalOntology;
        OWLOntology localOntology1;
        OWLOntology localOntology2;

        try {
            // load global ontology
            globalOntology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(mediator);

            // load local ontology 1
            localOntology1 = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(source1);

            // load local ontology 2
            localOntology2 = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(source2);
        } catch (OWLOntologyCreationException ex) {
            ex.printStackTrace();
            return;
        }

        // test the GAV mapping model
        MappingModel mappingModel = deserializer.deserialize(gavMappings);
        mappingModel.getGlobalOntology().setSchema(globalOntology);
        mappingModel.getLocalOntologyList().get(0).setSchema(localOntology1);
        mappingModel.getLocalOntologyList().get(1).setSchema(localOntology2);

        MappingReasoner reasoner = new MappingReasoner();
        List<Mapping> result = reasoner.computeInfMappings(mappingModel);
        mappingModel.getMappingList().addAll(result);

        // test the LAV mapping model
        mappingModel = deserializer.deserialize(lavMappings);
        mappingModel.getGlobalOntology().setSchema(globalOntology);
        mappingModel.getLocalOntologyList().get(0).setSchema(localOntology1);
        mappingModel.getLocalOntologyList().get(1).setSchema(localOntology2);

        reasoner = new MappingReasoner();
        result = reasoner.computeInfMappings(mappingModel);
        mappingModel.getMappingList().addAll(result);

        Serializer serializer = new Serializer();
        serializer.serialize(mappingModel, new File("/home/kostas/Desktop/mappings.xml"));

        // test the GLAV mapping model
        mappingModel = deserializer.deserialize(glavMappings);
        mappingModel.getGlobalOntology().setSchema(globalOntology);
        mappingModel.getLocalOntologyList().get(0).setSchema(localOntology1);
        mappingModel.getLocalOntologyList().get(1).setSchema(localOntology2);

        reasoner = new MappingReasoner();
        result = reasoner.computeInfMappings(mappingModel);
        mappingModel.getMappingList().addAll(result);
    }
}
