/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.mapping.model;

import java.io.File;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

/**
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class OWLConverterTest extends TestCase {

    public OWLConverterTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of convert method, of class OWLConverter.
     */
    public void testConvert() {
        System.out.println("convert");

        Deserializer deserializer = new Deserializer();
        OWLConverter converter = new OWLConverter();

        File gavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/mappings/gav.xml"));
        File lavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/mappings/lav.xml"));
        File glavMappings = FileUtils.toFile(getClass().getClassLoader().getResource("scenario1/mappings/glav.xml"));

        // convert GAV mappings
        MappingModel model = deserializer.deserialize(gavMappings);
        OWLOntology ontology = converter.convert(model);

        // convert LAV mappings
        model = deserializer.deserialize(lavMappings);
        ontology = converter.convert(model);

        // convert GLAV mappings
        model = deserializer.deserialize(glavMappings);
        ontology = converter.convert(model);
    }

    /**
     * Stores the contents of an ontology to an RDF/XML file.
     *
     * @param ontology The ontology to be stored.
     * @param file     A file.
     */
    private void serializeOntology(OWLOntology ontology, File file) {
        try {
            ontology.getOWLOntologyManager().saveOntology(ontology, IRI.create(file.toURI()));
        } catch (OWLOntologyStorageException ex) {
            ex.printStackTrace();
        }
    }
}
